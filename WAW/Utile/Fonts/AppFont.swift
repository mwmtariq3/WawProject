//
//  AppFont.swift
//  WAW
//
//  Created by Abed AlRhman Qasim on 6/1/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit

enum AppFont: String {
    case light = "Light"
    case regular = "Regular"
    case bold = "Bold"

    func size(_ size: CGFloat,TypeFont:Type) -> UIFont {
        if TypeFont == .helveticaFamilyName {
            if let font = UIFont(name: TypeFont.rawValue, size: size + 1.0) {
                return font
            }
        }else {
            if let font = UIFont(name: rawValue.isEmpty ? TypeFont.rawValue :  TypeFont.rawValue  + rawValue, size: size + 1.0) {
                return font
            }
        }
        return .boldSystemFont(ofSize: size)
    }
}
enum Type: String {
    case nexafamilyName =  "NexaDemo-"
    case helveticaFamilyName = "HelveticaNeueLTArabic-Roman"
}
