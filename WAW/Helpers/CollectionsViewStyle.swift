//
//  TableViewStyle.swift
//  Sharq
//
//  Created by Wafaa Alshahed on 1/20/18.
//  Copyright © 2018 Sharq. All rights reserved.
//

import UIKit

final class CollectionsViewStyle {
    
    static func tableViewNoDataStyle (tableView : UITableView , isDataAvailable : Bool, displayMsg: String = "No Data Available".localiz() ){
        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width - 40 , height: tableView.bounds.size.height))
        noDataLabel.numberOfLines = 0
        noDataLabel.text             = displayMsg
        noDataLabel.font             = UIFont(name: "Arial", size: 15)
        noDataLabel.textColor = #colorLiteral(red: 0.7332602739, green: 0.7333852649, blue: 0.7504467368, alpha: 1)
        noDataLabel.textAlignment    = .center
        tableView.backgroundView = isDataAvailable ? nil : noDataLabel
    }
    
    static func collectionNoDataStyle (collectionView : UICollectionView , isDataAvailable : Bool, displayMsg: String = "No Data Available".localiz() ){
        let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.size.width - 40 , height: collectionView.bounds.size.height))
        noDataLabel.text             = displayMsg
        noDataLabel.numberOfLines = 0
        noDataLabel.textColor = #colorLiteral(red: 0.7332602739, green: 0.7333852649, blue: 0.7504467368, alpha: 1)
        noDataLabel.font             = UIFont(name: "Arial", size: 15)
        noDataLabel.textAlignment    = .center
        collectionView.backgroundView = isDataAvailable ? nil : noDataLabel
    }
}



