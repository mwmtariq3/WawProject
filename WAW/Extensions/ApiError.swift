//
//  ApiError.swift
//  Mahalkom
//
//  Created by Wafaa Alshahed on 4/10/18.
//  Copyright © 2019 Line. All rights reserved.
//

import UIKit

class ApiError: NSObject {
    var code:Int?
    var message:String?
    
    init(code:Int,message:String) {
        self.code = code
        self.message = message
    }
    
}


