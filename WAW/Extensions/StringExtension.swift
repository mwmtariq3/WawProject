//
//  StringExtension .swift
//  Mahalkom
//
//  Created by Wafaa Alshahed on 5/20/18.
//  Copyright © 2018 Line. All rights reserved.
//

import Foundation

extension String {
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, options: [], range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
    
    var htmlAttributedString: NSAttributedString? {
        return try? NSAttributedString(data: Data(utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
    }
    
    var stringByRemovingWhitespaces: String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    var isAlphanumeric: Bool {
        return !(!isEmpty && range(of: "^(((?=.*[a-z].*)(?=.*\\d.*))|((?=.*[a-z].*)(?=.*[A-Z].))|((?=.*\\d.*)(?=.*[A-Z].))|((?=.*[a-z].*)(?=.*[^A-Za-z0-9].*))|((?=.*[^A-Za-z0-9].*)(?=.*[A-Z].))|((?=.*\\d.*)(?=.*[^A-Za-z0-9].*)))(?=.{8,})", options: .regularExpression) == nil)
    }
 


}

