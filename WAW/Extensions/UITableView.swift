//
//  UITableView.swift
//  Mahalkom
//
//  Created by Wafaa Alshahed on 2/27/18.
//  Copyright © 2019 Line. All rights reserved.
//

import UIKit

extension UITableView {
    
    func register<T:UITableViewCell>(_: T.Type){
        let bundle = Bundle(for: T.self)
        let nib =  UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forCellReuseIdentifier: T.nibName)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath)-> T{
        
        guard let cell = dequeueReusableCell(withIdentifier: T.nibName, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.nibName)")
        }
        return cell
    }
    
}

extension UITableViewCell:NibLoadableView{
    static var nibName:String {
        return String(describing: self)
    }
}
