

import UIKit
import LanguageManger_iOS

extension UIView{
    
    func addTapGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.addGestureRecognizer(tap)
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
            layer.borderWidth = 0.25
            layer.cornerRadius = 10
        }
        
    }

    override open func awakeFromNib() {
        switch self {
        case let txtf as UITextField:
            txtf.text = txtf.text?.localiz()
            txtf.placeholder = txtf.placeholder?.localiz()
            if txtf.textAlignment == .natural {
                txtf.semanticContentAttribute = LanguageManger.shared.currentLanguage == .ar ? .forceRightToLeft : .forceLeftToRight
                 txtf.textAlignment = LanguageManger.shared.currentLanguage == .ar ? .right : .left
            }
        case let lbl as UILabel:
            do {
                lbl.text = lbl.text?.localiz()
            }catch {
            }
        case let btn as UIButton:
            btn.clipsToBounds = true
            btn.setTitle(btn.title(for: .normal)?.localiz().uppercased(), for: .normal)
        default:
            break
        }
    }
  
    func slide(withDuration duration: TimeInterval) {
        guard let superViewWidth = self.superview?.frame.size.width else {
            
            return
        }
        let orginalFrame = self.frame
        var newFrame = orginalFrame
        newFrame.origin.x = newFrame.origin.x + superViewWidth
        self.frame = newFrame
        UIView.animate(withDuration: duration) {
            self.frame = orginalFrame
        }
    }

    
    func slideFromBottom(withDuration duration: TimeInterval) {
        guard let superViewHeight = self.superview?.frame.size.height else {
            return
        }
        let orginalFrame = self.frame
        var newFrame = orginalFrame
        newFrame.origin.y = newFrame.origin.y + superViewHeight
        self.frame = newFrame
        UIView.animate(withDuration: duration) {
            self.frame = orginalFrame
        }
    }
    
    func fadeIn(withDuration duration: TimeInterval,to alpha:CGFloat = 1) {
        self.alpha = 0
        UIView.animate(withDuration: duration) {
            self.alpha = alpha
        }
    }
    
    func fadeOut(withDuration duration: TimeInterval) {
        self.alpha = 1
        UIView.animate(withDuration: duration) {
            self.alpha = 0
        }
    }
    
    func rolling(withDuration duration: TimeInterval,delay:Double) {
        var transform = CATransform3DIdentity
        transform.m34 = 1.0 / -500.0
        transform = CATransform3DRotate(transform, (CGFloat(270.0 * Double.pi / 180.0)), 1, 0, 0);
        self.layer.transform = transform
        
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
            self.layer.transform = CATransform3DIdentity
        }) { _ in
            
        }
    }
    
    func changeBackgroundTo(color:UIColor){
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.backgroundColor = color
        }
    }
    
    internal func loadViewFromNib(viewClass: Swift.AnyClass) -> UIView! {
        let bundle = Bundle(for: viewClass.self)
        let nib = UINib(nibName: String(describing: viewClass.self), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      
        return view
    }
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
    
        self.layer.removeAnimation(forKey: "transform")
        self.layer.add(animation, forKey: "transform")
    }
    
    private static var _addShadow:Bool = false
    
    @IBInspectable var addShadow:Bool {
        get {
            return UIView._addShadow
        }
        set(newValue) {
            if(newValue == true){
                self.layer.masksToBounds = false
                self.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                self.layer.shadowOffset = CGSize(width: 0, height: 5)
                self.layer.shadowOpacity =  0.5
                self.layer.shadowRadius = 5.0
            }
        }
    }
    
    func clearSubviews()
    {
        for subview in self.subviews {
            subview.removeFromSuperview();
        }
    }
    
    func addFullScreenConstraints() {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|",
                                                                options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|",
                                                                options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
    }
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
}

extension String {
    var containsNonEnglishNumbers: Bool {
        return !isEmpty && range(of: "[^0-9]", options: .regularExpression) == nil
    }

    var english: String {
        return self.applyingTransform(StringTransform.toLatin, reverse: false) ?? self
    }
}

extension UIImage {

    func addImagePadding(x: CGFloat, y: CGFloat) -> UIImage? {
        let width: CGFloat = size.width + x
        let height: CGFloat = size.height + y
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 0)
        let origin: CGPoint = CGPoint(x: (width - size.width) / 2, y: (height - size.height) / 2)
        draw(at: origin)
        let imageWithPadding = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return imageWithPadding
    }
    func overlayWith(image: UIImage, posX: CGFloat, posY: CGFloat) -> UIImage {
        let newWidth = size.width < posX + image.size.width ? posX + image.size.width : size.width
        let newHeight = size.height < posY + image.size.height ? posY + image.size.height : size.height
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        draw(in: CGRect(origin: CGPoint.zero, size: size))
        image.draw(in: CGRect(origin: CGPoint(x: (size.width / 2) - (image.size.width / 2) , y:  (size.height / 2) - (image.size.height / 2) ), size: image.size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

