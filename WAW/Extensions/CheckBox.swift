//
//  CheckBox.swift
//  Mahalkom
//
//  Created by Wafaa Alshahed on 5/20/18.
//  Copyright © 2018 Line. All rights reserved.
//

import UIKit

class CheckBox: UIButton {
    // Images

    
    @IBInspectable var checkedImage : UIImage = UIImage(){
           didSet{
           self.setImage(checkedImage, for: UIControl.State.normal)
           }
       }
       
       @IBInspectable var uncheckedImage : UIImage = UIImage()
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
    
    var title:String = ""{
        didSet{
            self.setTitle(title, for: .normal)
            self.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
              self.contentHorizontalAlignment = .fill
            if #available(iOS 11.0, *) {
                self.contentHorizontalAlignment = .leading
            } else {
               self.contentHorizontalAlignment = .left
            }
        }
    }
    
    var buttonClickedBlock: (() -> ())?
    var buttonClickedValueBlock: ((Bool) -> ())?
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)       
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
        buttonClickedValueBlock? (isChecked)
        guard let actionBlock = buttonClickedBlock else { return }
        actionBlock ()
        
    }
}





