//
//  NotificationExtension.swift
//  Mahalkom
//
//  Created by Wafaa Alshahed on 12/4/18.
//  Copyright © 2018 Line. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let notifyCartChanged = Notification.Name("notifyCartChanged")
    static let unauthorizedUser = Notification.Name("unauthorizedUser")
}
extension String {
    
    func timeAgoSinceDate() -> String {
        if let timeStamp = Double(self) {
            let date = Date(timeIntervalSince1970: timeStamp)
            
            // From Time
            let fromDate = date
            
            // To Time
            let toDate = Date()
            
            // Estimation
            // Year
            if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {
                
                return interval == 1 ? "\(interval)" + " " + "year ago".localiz() : "\(interval)" + " " + "years ago".localiz()
            }
            
            // Month
            if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {
                
                return interval == 1 ? "\(interval)" + " " + "month ago".localiz() : "\(interval)" + " " + "months ago".localiz()
            }
            
            // Day
            if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {
                
                return interval == 1 ? "\(interval)" + " " + "day ago".localiz() : "\(interval)" + " " + "days ago".localiz()
            }
            
            // Hours
            if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {
                
                return interval == 1 ? "\(interval)" + " " + "hour ago".localiz() : "\(interval)" + " " + "hours ago".localiz()
            }
            
            // Minute
            if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {
                
                return interval == 1 ? "\(interval)" + " " + "minute ago".localiz() : "\(interval)" + " " + "minutes ago".localiz()
            }
        }
        return "a moment ago".localiz()
    }
}
