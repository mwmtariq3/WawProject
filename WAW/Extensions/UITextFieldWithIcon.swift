


import UIKit
import RSKPlaceholderTextView
import LanguageManger_iOS
import iOSDropDown

@IBDesignable
class UITextFiledWithIcon: UITextField {

    enum ImageDirection{
        case left
        case right
    }


    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }

    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }

    @IBInspectable var padding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var hasRoundCorners: Bool = true {
        didSet {
            updateView()
        }
    }



    @IBInspectable var navigateToViewControll: String?


    @IBInspectable var isRequired: Bool = false {
        didSet {
            if isRequired {
                placeholder = placeholder?.localiz() ?? "" + "*"
                //            var range = (placeholder! as NSString).range(of: "*")
                //            var attributedString = NSMutableAttributedString(string: "*")
                //            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
                //
                let rangeOfSubString = ((placeholder ?? "") as NSString).range(of: "*")
                let rangeOfFullString = NSRange(location: 0, length: placeholder?.count ?? 0)
                let attributedString = NSMutableAttributedString(string: placeholder ?? "")
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: tintColor, range: rangeOfFullString)
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: rangeOfSubString)
                attributedPlaceholder = attributedString
            }
        }
    }

    @IBInspectable var leftIconWidth: CGFloat = 20 {
        didSet {
            updateView()
        }
    }

    @IBInspectable var rightIconWidth: CGFloat = 20 {
        didSet {
            updateView()
        }
    }


    func updateView() {
        layer.cornerRadius = hasRoundCorners ? frame.height / 2 : 10

        // Placeholder text color
        if let placeholderString = placeholder {
            attributedPlaceholder = NSAttributedString(string: placeholderString.localiz(), attributes:[NSAttributedString.Key.foregroundColor: tintColor])
            placeholder = placeholder?.localiz()
        }


        textAlignment = LanguageManger.shared.currentLanguage == .ar ? .right : .left


        textColor = tintColor
        addIconToTextFiled(image: leftImage, imageDirection: .left)
        addIconToTextFiled(image: rightImage, imageDirection: .right)

    }

    func setRightImage(image: UIImage){
        addIconToTextFiled(image: image, imageDirection: .right)
    }

    func addIconToTextFiled(image: UIImage?, imageDirection : ImageDirection ){
        if let image = image {
            let pad : CGFloat = 20
            let  iconWidth = imageDirection == .left ? leftIconWidth : rightIconWidth
            let imageView = UIImageView(frame: CGRect(x: pad , y: -2, width: iconWidth, height: iconWidth))

            imageView.image  = image
            imageView.tintColor = tintColor
            imageView.contentMode = .scaleAspectFit

            var width = padding + iconWidth
            if borderStyle == .none || borderStyle == .line {
                width += 5
            }

            let view = UIView(frame: CGRect(x: pad, y: 0, width: width + pad , height: iconWidth))
            view.addSubview(imageView)

            switch imageDirection {
            case .left :
                leftViewMode = .always
                leftView = view
            case .right :
                rightViewMode = .always
                rightView = view
            }

        } else {

            switch imageDirection {
            case .left :
                leftViewMode = .always
                leftView = UIView(frame: CGRect(x: padding, y: 0, width: padding, height: frame.height))

            case .right :
                leftViewMode = .always
                rightView = UIView(frame: CGRect(x: padding , y: 0, width: padding, height: frame.height))
            }
        }
    }
}

//MARK:- CustomTextView
class CustomTextView:RSKPlaceholderTextView{

    let iconWidth: CGFloat = 20

    /// A UIImage value that set LeftImage to the UITextView
    @IBInspectable open var leftImage:UIImage? {
        didSet {
            updateView()
        }
    }

    @IBInspectable var padding: CGFloat = 20

    @IBInspectable var corners: CGFloat = 20 {
        didSet{
            layer.cornerRadius = corners
        }
    }

    @IBInspectable var isRequired: Bool = false

    @IBInspectable var txtPlaceholder: NSString? {
        didSet{
            if isRequired {
                var placeholderStr = String(txtPlaceholder ?? "")
                placeholderStr = placeholderStr.localiz() + "*"
                let rangeOfSubString = (placeholderStr as NSString).range(of: "*")
                let rangeOfFullString = NSRange(location: 0, length: placeholderStr.count)
                let attributedString = NSMutableAttributedString(string: placeholderStr)
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: tintColor, range: rangeOfFullString)
                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: rangeOfSubString)
                attributedPlaceholder = attributedString
            }else{
                let placeholderStr = String(txtPlaceholder ?? "").localiz()
                self.placeholder = "\(placeholderStr)"  as NSString
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        textAlignment = LanguageManger.shared.currentLanguage == .ar ? .right : .left
    }

    fileprivate func updateView() {
        if let image = leftImage {
            let imageView = UIImageView(frame: CGRect(x:  padding , y: padding , width: iconWidth, height: iconWidth))
            imageView.image = image
            imageView.tintColor = tintColor

            let width = padding * 1.5   + iconWidth
            let x = LanguageManger.shared.currentLanguage == .ar ? self.bounds.maxX - ( width + iconWidth / 2) :  self.bounds.minX
            let view = UIView(frame: CGRect(x: x , y: 0, width: width, height: width ))
            view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            view.addSubview(imageView)
            self.addSubview(view)


            if  LanguageManger.shared.currentLanguage == .ar  {
                self.textContainerInset = UIEdgeInsets(top: iconWidth , left:  2.0  , bottom: 2.0, right: width)
            }else{
                self.textContainerInset = UIEdgeInsets(top: iconWidth , left: width  , bottom: 2.0, right: 2.0)
            }


        }else{
            let view = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: iconWidth + 15 ))
            self.addSubview(view)
            self.textContainerInset = UIEdgeInsets(top: iconWidth , left: padding  , bottom: 2.0, right: 2.0)
        }
    }
}

extension DropDown {

    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
