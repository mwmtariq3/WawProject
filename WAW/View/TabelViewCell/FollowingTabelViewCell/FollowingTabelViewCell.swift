//
//  FollowingTabelViewCell.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/16/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit

class FollowingTabelViewCell: UITableViewCell {

    @IBOutlet weak var unfollowButton: UIButton!
    @IBOutlet weak var businseLabel: UILabel!
    @IBOutlet weak var shopLabel: UITextFiledWithIcon!
    @IBOutlet weak var retailLabel: UITextFiledWithIcon!
    @IBOutlet weak var followinImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
