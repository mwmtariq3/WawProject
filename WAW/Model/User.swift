//
//  User.swift
//  Farhati
//


import Foundation

struct User: Codable {
 
    let id: Int?
    let name: String?
    var email: String?
    let mobile: String?
    let userType:String?
    let token:String?
    let countryId:Int?
    let cityId:Int?
    let lang:String?
    let userStatus:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case email = "Email"
        case mobile = "Mobile"
        case userType = "UserType"
        case token = "Token"
        case countryId = "CountryId"
        case cityId = "CityId"
        case lang  = "Lang"
        case userStatus = "UserStatus"
    }
}
