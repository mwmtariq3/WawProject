//
//  GenericModel.swift
//  Tam
//
//  Created by Abed Nayef Qasim on 3/3/19.
//  Copyright © 2019 Abed Nayef Qasim. All rights reserved.
//

import Foundation

struct GenericModel<T:Codable>: Codable{
    let statusCode: Int?
    let msg: [String]?
    let successMessage:String?
    let data : T?
    enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case msg = "ErrorList"
        case data = "Data"
        case successMessage = "SuccessMessage"
    }
}

