//
//  Country.swift
//  StormCenter
//
//  Created by Abed AlRhman Qasim on 5/25/20.
//  Copyright © 2020 Abed Al-Rhman Qasim. All rights reserved.
//

import Foundation
struct Country: Codable {
    let id: Int?
    let name: String?
    let currency:String?
    let countryCode:String?
    let countryWithCode:String?
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case currency = "Currency"
        case countryCode = "Code"
        case countryWithCode = "CountryWithCode"

    }
}
