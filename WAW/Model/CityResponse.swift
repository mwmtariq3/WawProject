//
//  CityResponse.swift
//  StormCenter
//
//  Created by Abed AlRhman Qasim on 5/20/20.
//  Copyright © 2020 Abed Al-Rhman Qasim. All rights reserved.
//

import Foundation
struct City: Codable {
    let id: Int?
    let cityName: String?
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case cityName = "Name"
    }
}

