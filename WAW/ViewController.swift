//
//  ViewController.swift
//  StormCenter
//
//  Created by Abed Al-Rhman Qasim on 5/4/20.
//  Copyright © 2020 Abed Al-Rhman Qasim. All rights reserved.
//

import UIKit
import AVKit
import YPImagePicker
class ViewController: UIViewController {
    func noPhotos() {
        
    }
    
    var player: AVPlayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        loadVideo()
    }
    private func loadVideo() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient)
        } catch { }
        guard let path = Bundle.main.path(forResource: "splash", ofType:"mp4") else {return}
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        player = AVPlayer(url: NSURL(fileURLWithPath: path) as URL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        self.view.layer.addSublayer(playerLayer)
        player?.seek(to: CMTime.zero)
        player?.play()

    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func playerDidFinishPlaying(note: NSNotification) {
        
    }

}

