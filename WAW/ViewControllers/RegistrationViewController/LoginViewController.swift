//
//  LoginViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/9/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit
import LanguageManger_iOS

class LoginViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LanguageManger.shared.defaultLanguage = .en
    }
}
