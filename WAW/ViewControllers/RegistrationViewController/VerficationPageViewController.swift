//
//  VerficationPageViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/10/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit

class VerficationPageViewController: UIViewController , UITextFieldDelegate{
    @IBOutlet weak var verfyCodeTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var stackTermsAndCondtion: UIStackView!
    @IBOutlet weak var verfiyButton: UIButton!
    var fromRegisterBo = false
    var count = 60
    var timer: Timer?
    var isStopTimer = false
    var email = ""
    var code = ""
    var time = ""
    var error = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        generalCustomization()
    }
    func generalCustomization() {
        startTimer()
        code = ""
        verfyCodeTextFiled.delegate = self
        emailButton.setTitle(email, for: .normal)
        updateView()
        title = "VERIFICATION CODE".localiz()
    }
    
    @IBAction func resendCodeAction(_ sender: Any) {
        let fullPath:String = "\(Constants.urls.authUrls.forgetPassword)?EmailOrPhoneNumber=\(email)&ValidateType=1"
        SendAPIPost(url: fullPath, data: [:], decodable: GenericModel<Empty>.self) { response in
            if response.msg?.count ?? 0 != 0 {
                self.showToast(message: response.msg?.joined(separator: "\n") ?? "")
                return
            }
            self.startTimer()
        }
    }
    @IBAction func editEmailAction(_ sender: Any) {
        
    }
    
    @IBAction func Terms(_ sender: Any) {
    }
    @IBAction func verifyButtonAction(_ sender: Any) {
        if !isValid() {
            showToast(message: error)
            return
        }
        code = verfyCodeTextFiled.text ?? ""
        let fullPath = "\(Constants.urls.authUrls.activateUserByActivationCode)?Email=\(email)&Code=\(code.replacedArabicDigitsWithEnglish)"
        if fromRegisterBo {
            SendAPIPost(url: fullPath, data: [:], decodable: GenericModel<User>.self) { (response) in
                if response.msg?.count ?? 0 != 0 {
                    if response.msg?.contains(Constants.alreadyActive) ?? false {
                        Helper.currentUser = response.data
                        self.presentHome()
                        return
                    }
                    self.showToast(message: response.msg?.joined(separator: "\n") ?? "")
                    return
                }
                Helper.currentUser = response.data
                self.presentHome()
            }
        }else {
            SendAPIPost(url: fullPath, data: [:], decodable: GenericModel<Empty>.self) { (response) in
                if response.msg?.count ?? 0 != 0 {
                    if response.msg?.contains(Constants.alreadyActive) ?? false {
                        let vc = self.initViewControllerWith(identifier: "CreateNewPasswordViewController", and: "") as! CreateNewPasswordViewController
                        vc.email = self.email
                        self.show(vc)
                        return
                    }
                    self.showToast(message: response.msg?.joined(separator: "\n") ?? "")
                    return
                }
                let vc = self.initViewControllerWith(identifier: "CreateNewPasswordViewController", and: "") as! CreateNewPasswordViewController
                vc.email = self.email
                self.show(vc)
            }
        }
        
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateView()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateView()
    }
    func updateView() {
        if   !(verfyCodeTextFiled.text ?? "").isEmpty  {
            verfiyButton.tintColor = #colorLiteral(red: 0.9098039216, green: 0.2274509804, blue: 0.2705882353, alpha: 1)
        }else {
            verfiyButton.tintColor = #colorLiteral(red: 0.6588235294, green: 0.6509803922, blue: 0.6509803922, alpha: 1)
        }
    }
    func isValid() -> Bool {
        error = ""
        guard let verfy = verfyCodeTextFiled.text , !verfy.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if isStopTimer {
            error = "لتفعيل رمز جديد يرجى الضغط على إعادة الإرسال".localiz()
            return error.isEmpty
            
        }
        return error.isEmpty
    }
    func updateViewTimer() {
        resendButton.isHidden = !isStopTimer
        counterLabel.isHidden = isStopTimer
        
    }
    func startTimer() {
        stopTimer()
        isStopTimer = false
        updateViewTimer()
        time = ""
        count = 60
        guard self.timer == nil else { return }
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    func resumeTimer() {
        time = ""
        guard self.timer == nil else { return }
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    func stopTimer() {
        time = "00"
        count = 0
        code = ""
        isStopTimer = true
        verfyCodeTextFiled.text = ""
        updateViewTimer()
        guard timer != nil else { return }
        timer?.invalidate()
        timer = nil
    }
    
    @objc func update() {
        guard timer != nil else { return }
        if(count > 0) {
            let minutes = count / 60
            let seconds = count % 60
            if (minutes <= 0) {
                if seconds <= 0 {
                    time = "00"
                } else if seconds < 10 {
                    time = "0\(seconds)"
                } else {
                    time = "\(seconds)"
                }
            }
            count = count - 1
        } else {
            stopTimer()
        }
        counterLabel.text = time
    }
}
