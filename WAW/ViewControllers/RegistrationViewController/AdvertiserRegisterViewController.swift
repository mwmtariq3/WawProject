//
//  AdvertiserRegisterViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/11/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit
import iOSDropDown

class AdvertiserRegisterViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var owneryCountryDropDown: DropDown!
    @IBOutlet weak var ownerFirstNameTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var ownerGenderTextFiled: DropDown!
    @IBOutlet weak var ownerCityTextFiled: DropDown!
    @IBOutlet weak var countryCodeDropDown: UITextFiledWithIcon!
    @IBOutlet weak var mobileNumberTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var sendButton:UIButton!
    @IBOutlet weak var hightOfTabelView: NSLayoutConstraint!
    @IBOutlet weak var countryTabelView: UITableView!
    @IBOutlet weak var countrySearchBar: UITextFiledWithIcon!
    @IBOutlet weak var viewOfSearch: UIView!
    var countryCode = ""
    var selectedCountry = ""
    var countyId:Int = -1
    var cityId:Int = -1
    var error = ""
    var genederIndex = 0
    var dict:[String:Any] = [:]
    var searchText = String() {
        didSet{
            if searchText == "" {
                self.dataArray = self.optionArray
            }else{
                self.dataArray = optionArray.filter {
                    return $0.name?.range(of: searchText, options: .caseInsensitive) != nil
                }
            }
            setTableViewHeight()
        }
    }
    var dataArray:[Country] = []
    var optionArray = [Country]() {
        didSet{
            self.dataArray = self.optionArray
        }
    }
    var cityList:[City] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalCustomization()
        countryTabelView.register(TitleTabelViewCell.self)
        countryCodeDropDown.delegate = self
        countryCodeDropDown.text = ""
        title = "ADVERTISER ACCOUNT".localiz()
        setupCountry()
    }
    func setupCountry() {
        if Constants.countryList.count == 0 {
            getCountry {
                self.optionArray = Constants.countryList
            }
        }else {
            self.optionArray = Constants.countryList
        }
        
    }
    func setupCity(countryId:Int) {
        getCityById(cityId: countryId) { (cityList) in
            self.cityList = cityList
            if self.cityList.count > 0  {
                self.ownerCityTextFiled.text = self.cityList[0].cityName ?? ""
                self.cityId = self.cityList[0].id ?? 1
            }else {
                self.ownerCityTextFiled.text  = ""
                self.cityId = -1
            }
            self.ownerCityTextFiled.optionArray = self.cityList.map({ (city) -> String in
                return city.cityName ?? ""
            })
        }
        self.ownerCityTextFiled.didSelect { (item,index, i) in
            self.cityId = self.cityList[index].id ?? 1
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == countryCodeDropDown {
            return false
        }
        return true
    }
    @IBAction func didChange(_ sender: UITextFiledWithIcon) {
        searchText = sender.text ?? ""
    }
    @IBAction func didEdit(_ sender: UITextFiledWithIcon) {
        searchText = sender.text ?? ""
    }
    func setTableViewHeight(){
        let lableHeightAndMargeins: CGFloat = 430
        let maxHeight = view.frame.height - lableHeightAndMargeins
        let contentHeight = ((dataArray.count+1) * 50 )
        hightOfTabelView.constant = contentHeight > Int(maxHeight) ? maxHeight : CGFloat(contentHeight)
        self.countryTabelView.reloadData()
    }
    
    @IBAction func selectCountryAction(_ sender: CheckBox) {
        searchText = ""
        if !dataArray.isEmpty{
            self.viewOfSearch.isHidden = sender.isChecked
        }
        
    }
    
    
    @IBAction func sendAction(_ sender: Any) {
        if !isValid() {
            showToast(message: error)
            return
        }
        dict["CountryId"] = countyId
        dict["CountryCode"] = countryCode
        dict["CityId"] = cityId
        dict["Name"] = ownerFirstNameTextFiled.text ?? ""
        dict["Gender"] = genederIndex
        dict["Mobile"] = mobileNumberTextFiled.text ?? ""
        dict["AcceptCondition"] = "\(true)"
        AuthBO(dict: dict) {
            let vc = self.initViewControllerWith(identifier: "VerficationPageViewController", and: "") as! VerficationPageViewController
            vc.email = self.dict["Email"] as? String ?? ""
            vc.fromRegisterBo = true
            self.show(vc)
        }
    }
    
    func generalCustomization()  {
        ownerFirstNameTextFiled.delegate = self
        mobileNumberTextFiled.delegate = self
        ownerGenderTextFiled.delegate = self
        owneryCountryDropDown.delegate = self
        ownerCityTextFiled.delegate = self
        countryCodeDropDown.delegate = self
        countryCodeDropDown.text = "-"
        
        ownerGenderTextFiled.optionArray = ["Male".localiz(),"Female".localiz(),"Rather not to say".localiz()]
        ownerGenderTextFiled.didSelect { (item, index, drop) in
            self.genederIndex = index
        }
        title = "ADVERTISER ACCOUNT".localiz()
        updateView()
    }
    
    func updateView() {
        if   !(ownerFirstNameTextFiled.text ?? "").isEmpty && !(mobileNumberTextFiled.text ?? "").isEmpty && !(ownerGenderTextFiled.text ?? "").isEmpty &&
            !(owneryCountryDropDown.text ?? "").isEmpty &&
            !(ownerCityTextFiled.text ?? "").isEmpty &&
            !(countryCodeDropDown.text ?? "").isEmpty {
            sendButton.tintColor = #colorLiteral(red: 0.9098039216, green: 0.2274509804, blue: 0.2705882353, alpha: 1)
        }else {
            sendButton.tintColor = #colorLiteral(red: 0.6588235294, green: 0.6509803922, blue: 0.6509803922, alpha: 1)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateView()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateView()
    }
    func isValid() -> Bool {
        error = ""
        guard let owneryCountry = owneryCountryDropDown.text , !owneryCountry.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        guard let ownerFirstName = ownerFirstNameTextFiled.text , !ownerFirstName.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        guard let mobileNumber = mobileNumberTextFiled.text , !mobileNumber.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        guard let ownerGender = ownerGenderTextFiled.text , !ownerGender.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if countyId == -1 {
            error = "يرجى اختيار الدولة".localiz()
            return error.isEmpty
        }
        if cityId == -1 {
            error = "يرجى اختيار المدينة".localiz()
            return error.isEmpty
        }
        return error.isEmpty
    }
}
extension AdvertiserRegisterViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TitleTabelViewCell = tableView.dequeueReusableCell(withIdentifier: "TitleTabelViewCell") as! TitleTabelViewCell
        cell.backgroundColor = dataArray[indexPath.row].name != selectedCountry ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        cell.titleLabel.text = "\(dataArray[indexPath.row].name ?? "")"
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countrySearchBar.resignFirstResponder()
        countyId = self.dataArray[indexPath.row].id ?? -1
        owneryCountryDropDown.text = self.dataArray[indexPath.row].name
        self.countryCodeDropDown.text = self.dataArray[indexPath.row].countryCode ?? ""
        self.countryCode = self.dataArray[indexPath.row].countryCode ?? ""
        self.setupCity(countryId:  countyId)
        searchText.removeAll()
        countrySearchBar.text?.removeAll()
        viewOfSearch.isHidden = true
        selectedCountry = dataArray[indexPath.row].name ?? ""
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}

