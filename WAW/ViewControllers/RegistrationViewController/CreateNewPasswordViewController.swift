//
//  CreateNewPasswordViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/10/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit

class CreateNewPasswordViewController: UIViewController {
    @IBOutlet weak var newPasswordTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var confirmNewPaswwordTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var passwordInvisableButton: CheckBox!
    @IBOutlet weak var confirmPasswordInvisableButton: CheckBox!
    
    var error = ""
     var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordInvisableButton.isChecked = false
        confirmPasswordInvisableButton.isChecked = false
         title = "NEW PASSWORD".localiz()
    }
    
    @IBAction func passwordInvisableAction(_ sender: CheckBox) {
        switch sender {
        case passwordInvisableButton:
            newPasswordTextFiled.isSecureTextEntry = passwordInvisableButton.isChecked
            passwordInvisableButton.tintColor = passwordInvisableButton.isChecked ? #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) : #colorLiteral(red: 0.8042808771, green: 0.09774150699, blue: 0.1986472607, alpha: 1)
            
        case confirmPasswordInvisableButton:
            confirmNewPaswwordTextFiled.isSecureTextEntry = confirmPasswordInvisableButton.isChecked
            confirmPasswordInvisableButton.tintColor = confirmPasswordInvisableButton.isChecked ? #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) : #colorLiteral(red: 0.8042808771, green: 0.09774150699, blue: 0.1986472607, alpha: 1)
        default:
            print("")
        }
    }

    @IBAction func saveButtonAction(_ sender: Any) {
        if !isValid() {
            showToast(message: error)
            return
        }
 
        let fullPath = "\(Constants.urls.authUrls.changePasswordAfterVerification)?Email=\(email)&Password=\(newPasswordTextFiled.text!)&ConfirmPassword=\(confirmNewPaswwordTextFiled.text!)"
        SendAPIPost(url: fullPath, data: [:], decodable: GenericModel<Empty>.self) { (response) in
            if response.msg?.count ?? 0 != 0 {
                self.showToast(message: response.msg?.joined(separator: "\n") ?? "")
                return
            }
            MainLoginViewController.openLogin = true
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    func isValid() -> Bool {
        error = ""
        
        guard let password = newPasswordTextFiled.text , !password.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if password.count < 6  {
            error = "يرجى التحقق من كلمة المرور".localiz()
            return error.isEmpty
        }
        guard let confirmPassword = confirmNewPaswwordTextFiled.text , !confirmPassword.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if confirmPassword.count < 6  {
            error = "يرجى التحقق من تأكيد كلمة المرور".localiz()
            return error.isEmpty
        }
        if password != confirmPassword {
            error = "كلمة المرور غير متطابقة".localiz()
            return error.isEmpty
        }
        return error.isEmpty
    }

}

