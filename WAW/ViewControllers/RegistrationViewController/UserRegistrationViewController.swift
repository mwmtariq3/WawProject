//
//  UserRegistrationViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/10/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit
import iOSDropDown
class UserRegistrationViewController: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var countryTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var cityTextFiled: DropDown!
    @IBOutlet weak var emailTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var passwordTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var confirmPasswordTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var nextButton:UIButton!
    @IBOutlet weak var passwordInvisableButton: CheckBox!
    @IBOutlet weak var confirmPasswordInvisableButton: CheckBox!
    @IBOutlet weak var hightOfTabelView: NSLayoutConstraint!
    @IBOutlet weak var countryTabelView: UITableView!
    @IBOutlet weak var countrySearchBar: UITextFiledWithIcon!
    @IBOutlet weak var viewOfSearch: UIView!
    var countryId = -1
    var countryCode = ""
    var cityId = -1
    var error = ""
    var isEnable = false
    var dict:[String:Any] = [:]
    var selectedCountry = ""
    var searchText = String() {
        didSet{
            if searchText == "" {
                self.dataArray = self.optionArray
            }else{
                self.dataArray = optionArray.filter {
                    return $0.name?.range(of: searchText, options: .caseInsensitive) != nil
                }
            }
            setTableViewHeight()
        }
    }
    var dataArray:[Country] = []
    var optionArray = [Country]() {
        didSet{
            self.dataArray = self.optionArray
        }
    }
    var cityList:[City] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        delegateFields()
        passwordInvisableButton.isChecked = false
        confirmPasswordInvisableButton.isChecked = false
        countryTabelView.register(TitleTabelViewCell.self)
        setupCountry()
    }
    func setupCountry() {
        if Constants.countryList.count == 0 {
            getCountry {
                self.optionArray = Constants.countryList
            }
        }else {
            self.optionArray = Constants.countryList
        }
        
    }
    func setupCity(countryId:Int) {
        getCityById(cityId: countryId) { (cityList) in
            self.cityList = cityList
            if self.cityList.count > 0  {
                self.cityTextFiled.text = self.cityList[0].cityName ?? ""
                self.cityId = self.cityList[0].id ?? 1
            }else {
                self.cityTextFiled.text  = ""
                self.cityId = -1
            }
            self.cityTextFiled.optionArray = self.cityList.map({ (city) -> String in
                return city.cityName ?? ""
            })
        }
        self.cityTextFiled.didSelect { (item,index, i) in
            self.cityId = self.cityList[index].id ?? 1
        }
    }
    func setTableViewHeight(){
        let lableHeightAndMargeins: CGFloat = 220
        let maxHeight = view.frame.height - lableHeightAndMargeins
        let contentHeight = ((dataArray.count+1) * 50 )
        hightOfTabelView.constant = contentHeight > Int(maxHeight) ? maxHeight : CGFloat(contentHeight)
        self.countryTabelView.reloadData()
    }
    @IBAction func selectCountryAction(_ sender: CheckBox) {
        searchText = ""
        if !dataArray.isEmpty{
            self.viewOfSearch.isHidden = sender.isChecked
        }
        
    }
    @IBAction func didBegin(_ sender: UITextFiledWithIcon) {
        searchText = sender.text ?? ""
        
    }
    @IBAction func didChange(_ sender: UITextFiledWithIcon) {
        searchText = sender.text ?? ""
        
    }
    
    @IBAction func passwordInvisableAction(_ sender: CheckBox) {
        switch sender {
        case passwordInvisableButton:
            passwordTextFiled.isSecureTextEntry = passwordInvisableButton.isChecked
            passwordInvisableButton.tintColor = passwordInvisableButton.isChecked ? #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) : #colorLiteral(red: 0.8042808771, green: 0.09774150699, blue: 0.1986472607, alpha: 1)
            
        case confirmPasswordInvisableButton:
            confirmPasswordTextFiled.isSecureTextEntry = confirmPasswordInvisableButton.isChecked
            confirmPasswordInvisableButton.tintColor = confirmPasswordInvisableButton.isChecked ? #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) : #colorLiteral(red: 0.8042808771, green: 0.09774150699, blue: 0.1986472607, alpha: 1)
        default:
            print("")
        }
        
        
    }
    func delegateFields()  {
        countryTextFiled.delegate = self
        cityTextFiled.delegate = self
        emailTextFiled.delegate = self
        passwordTextFiled.delegate = self
        confirmPasswordTextFiled.delegate = self
        navigationItem.titleView =  navTitleWithImageAndText(titleText: "REGISTER", imageView: #imageLiteral(resourceName: "email"))
        updateView()
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        if !isValid() {
            showToast(message: error)
            return
        }
        let dictDublication:[String:Any] = ["CheckType":0,"EmailOrPhoneNumber":emailTextFiled.text!]
        SendAPIPost(url: Constants.urls.authUrls.checkduplicatedata, data: dictDublication, decodable: GenericModel<Empty>.self) { resoponse in
            if resoponse.msg?.count ?? 0 != 0 {
                if resoponse.msg?.contains(Constants.duplicateEmailAddress) ?? false {
                    self.showalert(title: "Email Duplicate", message: "Sorry, that email already exists , Do you want to log in?".localiz(), okTitleButton: "Yes".localiz(), cancelTitleButton: "No".localiz()) {
                        MainLoginViewController.openLogin = true
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }else {
                    self.showToast(message: resoponse.msg?.joined(separator: "\n") ?? "")
                    
                }
                return
            }
            let dict:[String:Any] = ["RegisterType":0,"EmailOrPhoneNumber":self.emailTextFiled.text! , "Password":self.passwordTextFiled.text!, "ConfirmPassword":self.confirmPasswordTextFiled.text!,"CountryId":self.countryId,"CountryCode":self.countryCode,"CityId" :self.cityId]
            let vc = self.initViewControllerWith(identifier: "PersonalInfoViewController", and: "") as! PersonalInfoViewController
            vc.dict = dict
            self.show(vc)
        }
        
    }
    func updateView() {
        if  !(emailTextFiled.text ?? "").isEmpty && !(confirmPasswordTextFiled.text ?? "").isEmpty && !(passwordTextFiled.text ?? "").isEmpty && countryId != -1 && cityId != -1  {
            nextButton.tintColor = #colorLiteral(red: 0.9098039216, green: 0.2274509804, blue: 0.2705882353, alpha: 1)
        }else {
            nextButton.tintColor = #colorLiteral(red: 0.6588235294, green: 0.6509803922, blue: 0.6509803922, alpha: 1)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateView()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateView()
    }
    func isValid() -> Bool {
        error = ""
        if countryId == -1 {
            error = "يرجى اختيار الدولة".localiz()
            return error.isEmpty
        }
        if cityId == -1 {
            error = "يرجى اختيار المدينة".localiz()
            return error.isEmpty
        }
        guard let email = emailTextFiled.text , !email.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if !email.isValidEmail() {
            error = "يرجى التأكد من صيغة البريد الإلكتروني".localiz()
            return error.isEmpty
        }
        guard let password = passwordTextFiled.text , !password.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if password.count < 6  {
            error = "يرجى التحقق من كلمة المرور".localiz()
            return error.isEmpty
        }
        guard let confirmPassword = confirmPasswordTextFiled.text , !confirmPassword.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if confirmPassword.count < 6  {
            error = "يرجى التحقق من تأكيد كلمة المرور".localiz()
            return error.isEmpty
        }
        if password != confirmPassword {
            error = "كلمة المرور غير متطابقة".localiz()
            return error.isEmpty
        }
        return error.isEmpty
    }
}
extension UserRegistrationViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TitleTabelViewCell = tableView.dequeueReusableCell(withIdentifier: "TitleTabelViewCell") as! TitleTabelViewCell
        cell.backgroundColor = dataArray[indexPath.row].name != selectedCountry ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        cell.titleLabel.text = "\(dataArray[indexPath.row].name ?? "")"
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countrySearchBar.resignFirstResponder()
        countryTextFiled.text = self.dataArray[indexPath.row].name
        countryId = self.dataArray[indexPath.row].id ?? -1
        countryCode =   self.dataArray[indexPath.row].countryCode ?? ""
        self.setupCity(countryId: countryId)
        searchText.removeAll()
        countrySearchBar.text?.removeAll()
        viewOfSearch.isHidden = true
        selectedCountry = dataArray[indexPath.row].name ?? ""
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}

