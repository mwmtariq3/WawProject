//
//  NormalLoginViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/10/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices
import CryptoKit
import FirebaseAuth

class NormalLoginViewController: UIViewController ,UITextFieldDelegate{
    @IBOutlet weak var emailTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var passwordTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var advertiesersLogin: UIButton!
    @IBOutlet weak var invisableCheckedButton: CheckBox!
    var error = ""
    fileprivate var currentNonce: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "LOGIN".localiz()
        emailTextFiled.delegate = self
        passwordTextFiled.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        invisableCheckedButton.isChecked = false
        
    }
    
    @IBAction func passwordInvisableAction(_ sender: CheckBox) {
        passwordTextFiled.isSecureTextEntry = sender.isChecked
        invisableCheckedButton.tintColor = sender.isChecked ? #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) : #colorLiteral(red: 0.8042808771, green: 0.09774150699, blue: 0.1986472607, alpha: 1)
        
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        if !isValid() {
            showToast(message: error)
            return
        }
        resignTextField()
        let dict:[String:Any] = ["EmailOrPhoneNumber":emailTextFiled.text ?? "" , "Password":passwordTextFiled.text ?? ""]
        AuthNormal(dict: dict, viewController: self, isLogin: true) {
            self.presentHome()
        }
    }
    func resignTextField() {
        emailTextFiled.resignFirstResponder()
        passwordTextFiled.resignFirstResponder()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.updateFCMToken {}
    }
    
    @IBAction func ForegetPassword(_ sender: Any) {
        let vc = initViewControllerWith(identifier: "ResetPasswordViewController", and: "") as! ResetPasswordViewController
        show(vc)
    }
    @IBAction func Apple(_ sender: Any) {
        
        if #available(iOS 13.0, *) {
            startSignInWithAppleFlow()
        } else {
            self.showToast(message: "هذه الميزة مخصصة لنظام التشغيل iOS التي تحتوي على تحديث أعلى من 13".localiz())
        }
    }
    
    
    
    fileprivate func signIn(with credential: AuthCredential, type: String) {
        print("credential__", credential)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                
                self.showToast(message: error.localizedDescription)
                
                return
            }
            
            guard let result = authResult else {
                return
            }
            
            print("result__social", result)
            
            let name = result.user.displayName ?? ""
            let email = result.user.email ?? ""
            let accountID = result.user.uid
            let deviceToken = ""
            let accessToken = result.user.refreshToken
            let vc = self.initViewControllerWith(identifier: "FaceBookSignUpViewController", and: "") as! FaceBookSignUpViewController
            vc.isSoicalMedia = true
            self.show(vc)
            
        }
    }
    
    @IBAction func Facebook(_ sender: Any) {
        LoginManager().logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                self.showToast(message: error.localizedDescription)
                return
            }
            
            guard let token = result?.token else {
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: token.tokenString)
            self.signIn(with: credential, type: "1")
            
        }
    }
    @IBAction func Google(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signIn()
    }
    @IBAction func advirtismentLoginAction(_ sender: Any) {
        let vc = initViewControllerWith(identifier: "AdvertiserRegistrationViewController", and: "") as! AdvertiserRegistrationViewController
        show(vc)
    }
    
    @IBAction func ForgetPassword(_ sender: Any) {
        let vc = initViewControllerWith(identifier: "ResetPasswordViewController", and: "") as! ResetPasswordViewController
        show(vc)
    }
    func isValid() -> Bool {
        error = ""
        guard let email = emailTextFiled.text , !email.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if !email.isValidEmail() {
            error = "يرجى التأكد من صيغة البريد الإلكتروني".localiz()
            return error.isEmpty
        }
        guard let password = passwordTextFiled.text , !password.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        return error.isEmpty
    }
}
extension NormalLoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            // handle the error :)
            self.showToast(message: error.localizedDescription)
            return
        }
        
        guard let authentication = user.authentication else {
            return
        }
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        
        
        
        self.signIn(with: credential, type: "2")
    }
}

//Apple Sign In
@available(iOS 13.0, *)
extension NormalLoginViewController: ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding {
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    print("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return .init()
    }
    
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                print("Invalid state: A login callback was received, but no login request was sent.")
                return
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            // Initialize a Firebase credential.
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            self.signIn(with: credential, type: "3")
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("Sign in with Apple errored: \(error)")
    }
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
}
