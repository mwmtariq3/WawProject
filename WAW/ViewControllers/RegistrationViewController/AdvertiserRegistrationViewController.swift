//
//  AdvertiserRegistrationViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/14/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//


import UIKit

class AdvertiserRegistrationViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var ownerEmailTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var passwordTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var confirmPasswordTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var passwordInvisableButton: CheckBox!
    @IBOutlet weak var confirmPasswordInvisableButton: CheckBox!
    @IBOutlet weak var nextButton: UIButton!
    var dict:[String:Any] = [:]
    var error = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordInvisableButton.isChecked = false
        confirmPasswordInvisableButton.isChecked = false
        title = "ADVERTISER ACCOUNT"
    }
    
    @IBAction func passwordInvisableAction(_ sender: CheckBox) {
        switch sender {
        case passwordInvisableButton:
            passwordTextFiled.isSecureTextEntry = passwordInvisableButton.isChecked
            passwordInvisableButton.tintColor = passwordInvisableButton.isChecked ? #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) : #colorLiteral(red: 0.8042808771, green: 0.09774150699, blue: 0.1986472607, alpha: 1)
            
        case confirmPasswordInvisableButton:
            confirmPasswordTextFiled.isSecureTextEntry = confirmPasswordInvisableButton.isChecked
            confirmPasswordInvisableButton.tintColor = confirmPasswordInvisableButton.isChecked ? #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) : #colorLiteral(red: 0.8042808771, green: 0.09774150699, blue: 0.1986472607, alpha: 1)
        default:
            print("")
        }
        delegateFields()
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        if !isValid() {
            showToast(message: error)
            return
        }
        let dictDublication:[String:Any] = ["CheckType":0,"EmailOrPhoneNumber":ownerEmailTextFiled.text!]
        SendAPIPost(url: Constants.urls.authUrls.checkduplicatedata, data: dictDublication, decodable: GenericModel<Empty>.self) { resoponse in
            if resoponse.msg?.count ?? 0 != 0 {
                if resoponse.msg?.contains(Constants.duplicateEmailAddress) ?? false {
                    self.showalert(title: "Email Duplicate", message: "Sorry, that email already exists , Do you want to log in?".localiz(), okTitleButton: "Yes".localiz(), cancelTitleButton: "No".localiz()) {
                        MainLoginViewController.openLogin = true
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }else {
                    self.showToast(message: resoponse.msg?.joined(separator: "\n") ?? "")
                    
                }
                return
            }
           
            let dict:[String:Any] = ["RegisterType":0,"Email":self.ownerEmailTextFiled.text! , "Password":self.passwordTextFiled.text!, "ConfirmPassword":self.confirmPasswordTextFiled.text!]
            let vc = self.initViewControllerWith(identifier: "AdvertiserRegisterViewController", and: "") as! AdvertiserRegisterViewController
            vc.dict = dict
            self.show(vc)
        }
        
    }
    func delegateFields()  {
        ownerEmailTextFiled.delegate = self
        passwordTextFiled.delegate = self
        confirmPasswordTextFiled.delegate = self
        title = "ADVERTISER ACCOUNT".localiz()
        updateView()
    }
    func updateView() {
        if   !(ownerEmailTextFiled.text ?? "").isEmpty && !(confirmPasswordTextFiled.text ?? "").isEmpty && !(passwordTextFiled.text ?? "").isEmpty  {
            nextButton.tintColor = #colorLiteral(red: 0.9098039216, green: 0.2274509804, blue: 0.2705882353, alpha: 1)
        }else {
            nextButton.tintColor = #colorLiteral(red: 0.6588235294, green: 0.6509803922, blue: 0.6509803922, alpha: 1)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateView()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateView()
    }
    func isValid() -> Bool {
        error = ""
        
        guard let email = ownerEmailTextFiled.text , !email.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if !email.isValidEmail() {
            error = "يرجى التأكد من صيغة البريد الإلكتروني".localiz()
            return error.isEmpty
        }
        guard let password = passwordTextFiled.text , !password.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if password.count < 6  {
            error = "يرجى التحقق من كلمة المرور".localiz()
            return error.isEmpty
        }
        guard let confirmPassword = confirmPasswordTextFiled.text , !confirmPassword.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if confirmPassword.count < 6  {
            error = "يرجى التحقق من تأكيد كلمة المرور".localiz()
            return error.isEmpty
        }
        if password != confirmPassword {
            error = "كلمة المرور غير متطابقة".localiz()
            return error.isEmpty
        }
        return error.isEmpty
    }
    
}
