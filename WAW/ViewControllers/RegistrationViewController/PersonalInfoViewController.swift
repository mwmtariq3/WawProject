//
//  PersonalInfoViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/11/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit
import iOSDropDown

class PersonalInfoViewController: UIViewController {
    @IBOutlet weak var fullNameTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var countryCodeDropDown: DropDown!
    @IBOutlet weak var mobileNumberTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var ownerGenderTextFiled: DropDown!
    var dict:[String:Any] = [:]
    var genederIndex = 0
    var countryCode = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        ownerGenderTextFiled.optionArray = ["Male".localiz(),"Female".localiz(),"Rather not to say".localiz()]
        ownerGenderTextFiled.didSelect { (item, index, drop) in
            self.genederIndex = index
        }
        self.countryCodeDropDown.didSelect { (item, index, i) in
            self.countryCode = item
        }
        setupCode()
    }
    func setupCode() {
        if Constants.countryList.count == 0 {
            getCountry {
                self.countryCodeDropDown.optionArray = Constants.countryList.map({ (country) -> String in
                    return country.countryCode ?? ""
                })
            }
        }else {
            self.countryCodeDropDown.optionArray = Constants.countryList.map({ (country) -> String in
                return country.countryCode ?? ""
            })
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Personal Info.".localiz()
    }
    
    @IBAction func sendAction(_ sender: Any) {
        sendData()
    }
    
    @IBAction func skipForNowAction(_ sender: Any) {
        sendData()
    }
    func sendData() {
        dict["Name"] = fullNameTextFiled.text ?? ""
        dict["Gender"] = self.genederIndex
        dict["Mobile"] = self.mobileNumberTextFiled.text ?? ""
        dict["AcceptCondition"] = "\(true)"
        AuthNormal(dict: dict, viewController: self, isLogin: false) {}
    }
}
