//
//  ResetPasswordViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/10/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var emailTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var nextButton:UIButton!
    var error = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "RESET PASSWORD".localiz()
        emailTextFiled.delegate = self
        updateView()
    }
    func updateView() {
        if  !(emailTextFiled.text ?? "").isEmpty  {
            nextButton.tintColor = #colorLiteral(red: 0.9098039216, green: 0.2274509804, blue: 0.2705882353, alpha: 1)
        }else {
            nextButton.tintColor = #colorLiteral(red: 0.6588235294, green: 0.6509803922, blue: 0.6509803922, alpha: 1)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateView()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateView()
    }
    @IBAction func sendButtonAction(_ sender: Any) {
        if !isValid() {
            showToast(message: error)
            return
        }
        resignTextField()
        let fullPath:String = "\(Constants.urls.authUrls.forgetPassword)?EmailOrPhoneNumber=\(emailTextFiled.text ?? "")&ValidateType=1"
        SendAPIPost(url: fullPath, data: [:], decodable: GenericModel<Empty>.self) { (user) in
            if user.msg?.count ?? 0 != 0 {
                self.showToast(message: user.msg?.joined(separator: "\n") ?? "")
                return
            }
            let vc = self.initViewControllerWith(identifier: "VerficationPageViewController", and: "") as! VerficationPageViewController
            vc.email = self.emailTextFiled.text ?? ""
            self.show(vc)
        }
    }
    func resignTextField() {
        emailTextFiled.resignFirstResponder()
    }
    func isValid() -> Bool {
        error = ""
        guard let email = emailTextFiled.text , !email.isEmpty else {
            error = "يُرجى التأكد من الحقول الفارغة".localiz()
            return error.isEmpty
        }
        if !email.isValidEmail() {
            error = "يرجى التأكد من صيغة البريد الإلكتروني".localiz()
            return error.isEmpty
        }
        
        return error.isEmpty
    }
}
