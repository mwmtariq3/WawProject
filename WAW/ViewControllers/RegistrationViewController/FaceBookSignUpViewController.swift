//
//  FaceBookSignUpViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/10/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit
import iOSDropDown

class FaceBookSignUpViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var cityTextDropDown: DropDown!
    @IBOutlet weak var countryDropDown: UITextFiledWithIcon!
    @IBOutlet weak var hightOfTabelView: NSLayoutConstraint!
    @IBOutlet weak var countryTabelView: UITableView!
    @IBOutlet weak var countrySearchBar: UITextFiledWithIcon!
    @IBOutlet weak var viewOfSearch: UIView!
    var error = ""
    var countryId = -1
    var countryCode = ""
    var cityId = -1
    var searchText = String() {
        didSet{
            if searchText == "" {
                self.dataArray = self.optionArray
            }else{
                self.dataArray = optionArray.filter {
                    return $0.name?.range(of: searchText, options: .caseInsensitive) != nil
                }
            }
            setTableViewHeight()
        }
    }
    var optionArray = [Country]() {
        didSet{
            self.dataArray = self.optionArray
        }
    }
    var dataArray:[Country] = []
    var cityList:[City] = []
    var selectedCountry = ""
    var isSoicalMedia = false
    override func viewDidLoad() {
        super.viewDidLoad()
        title = isSoicalMedia ? "GUEST SIGN IN".localiz() : "FACEBOOK SIGN IN".localiz()
        setupCountry()
        countryTabelView.register(TitleTabelViewCell.self)
        
    }
    func setupCountry() {
        if Constants.countryList.count == 0 {
            getCountry {
                self.optionArray = Constants.countryList
            }
        }else {
            self.optionArray = Constants.countryList
        }
        
    }
    func setupCity(countryId:Int) {
        getCityById(cityId: countryId) { (cityList) in
            self.cityList = cityList
            if self.cityList.count > 0  {
                self.cityTextDropDown.text = self.cityList[0].cityName ?? ""
                self.cityId = self.cityList[0].id ?? 1
            }else {
                self.cityTextDropDown.text  = ""
                self.cityId = -1
            }
            self.cityTextDropDown.optionArray = self.cityList.map({ (city) -> String in
                return city.cityName ?? ""
            })
        }
        self.cityTextDropDown.didSelect { (item,index, i) in
            self.cityId = self.cityList[index].id ?? 1
        }
    }
    func setTableViewHeight(){
        let lableHeightAndMargeins: CGFloat = 220
        let maxHeight = view.frame.height - lableHeightAndMargeins
        let contentHeight = ((dataArray.count+1) * 50 )
        hightOfTabelView.constant = contentHeight > Int(maxHeight) ? maxHeight : CGFloat(contentHeight)
        self.countryTabelView.reloadData()
    }
    @IBAction func selectCountryAction(_ sender: CheckBox) {
        searchText = ""
        if !dataArray.isEmpty{
            self.viewOfSearch.isHidden = sender.isChecked
        }
        
    }
    
    @IBAction func didChange(_ sender: UITextFiledWithIcon) {
        searchText = sender.text ?? ""
    }
    @IBAction func didBegin(_ sender: UITextFiledWithIcon) {
        searchText = sender.text ?? ""
    }
    
    @IBAction func agreeAction(_ sender: Any) {
        if !isValid() {
            showToast(message: error)
            return
        }
        SendAPIPost(url: Constants.urls.authUrls.getGuestUser, data: ["CountryId":"\(countryId)","CityId":"\(cityId)"], decodable: GenericModel<User>.self) { (response) in
            if response.msg?.count ?? 0 != 0 {
                self.showToast(message: response.msg?.joined(separator: "\n") ?? "")
                return
            }
            Helper.currentUser = response.data
            Helper.userType = Constants.UserType.Guest.rawValue
            self.presentHome()
        }
    }
    func isValid() -> Bool {
        error = ""
        if countryId == -1 {
            error = "يرجى اختيار الدولة".localiz()
            return error.isEmpty
        }
        
        if cityId == -1 {
            error = "يرجى اختيار المدينة".localiz()
            return error.isEmpty
        }
        return error.isEmpty
    }
}


extension FaceBookSignUpViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TitleTabelViewCell = tableView.dequeueReusableCell(withIdentifier: "TitleTabelViewCell") as! TitleTabelViewCell
        cell.backgroundColor = dataArray[indexPath.row].name != selectedCountry ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        cell.titleLabel.text = "\(dataArray[indexPath.row].name ?? "")"
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countrySearchBar.resignFirstResponder()
        countryDropDown.text = self.dataArray[indexPath.row].name
        countryCode = self.dataArray[indexPath.row].countryCode ?? ""
        self.setupCity(countryId:  self.dataArray[indexPath.row].id ?? -1)
        searchText.removeAll()
        countrySearchBar.text?.removeAll()
        viewOfSearch.isHidden = true
        selectedCountry = dataArray[indexPath.row].name ?? ""
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
