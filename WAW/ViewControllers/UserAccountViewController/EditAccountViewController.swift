//
//  EditAccountViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/16/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit
import iOSDropDown

class EditAccountViewController: UIViewController {
    
    @IBOutlet weak var genderDropDown: DropDown!
    @IBOutlet weak var fullNameTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var countryCodeDropDown: DropDown!
    @IBOutlet weak var mobileTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var countryDropDown: DropDown!
    @IBOutlet weak var cityDropDown: DropDown!
    @IBOutlet weak var myLanguageDropDown: DropDown!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changePasswordButton.setAttributedTitle(NSAttributedString(string: "Change Password".localiz(), attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue]), for: .normal)
    }
    @IBAction func saveAction(_ sender: Any) {
    }
    @IBAction func cancelAction(_ sender: Any) {
    }
    @IBAction func changePasswordAction(_ sender: Any) {
    }
}
