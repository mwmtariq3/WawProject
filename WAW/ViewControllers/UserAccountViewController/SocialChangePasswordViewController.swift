//
//  SocialChangePasswordViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/15/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit

class SocialChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var newPasswordTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var confirmNewPaswwordTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var newPasswordInvisableButton: CheckBox!
    @IBOutlet weak var confirmPasswordInvisableButton: CheckBox!
    
    var error = ""
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmPasswordInvisableButton.isChecked = false
        newPasswordInvisableButton.isChecked = false
        title = "CHANGE PASSWORD".localiz()
    }
    
    @IBAction func passwordInvisableAction(_ sender: CheckBox) {
        switch sender {
      
        case confirmPasswordInvisableButton:
            confirmNewPaswwordTextFiled.isSecureTextEntry = confirmPasswordInvisableButton.isChecked
            confirmPasswordInvisableButton.tintColor = confirmPasswordInvisableButton.isChecked ? #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) : #colorLiteral(red: 0.8042808771, green: 0.09774150699, blue: 0.1986472607, alpha: 1)
        case newPasswordInvisableButton:
            newPasswordTextFiled.isSecureTextEntry = newPasswordInvisableButton.isChecked
           newPasswordInvisableButton.tintColor = newPasswordInvisableButton.isChecked ? #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1) : #colorLiteral(red: 0.8042808771, green: 0.09774150699, blue: 0.1986472607, alpha: 1)
        default:
            print("")
        }
    }
    @IBAction func saveAction(_ sender: Any) {
    }
}
