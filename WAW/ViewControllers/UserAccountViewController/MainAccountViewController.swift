//
//  MainAccountViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/17/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit
import Tabman
import Pageboy

class MainMenuAccountViewController: TabmanViewController {
    
    
    
    var menuItem = [" My Accoun ".localiz()," Favorites List ".localiz()," Following List ".localiz()]
    private var viewControllers = [Helper.initViewControllerWith(identifier: "MainAccountViewController", and: ""), Helper.initViewControllerWith(identifier: "FavoriteListViewController", and: ""),Helper.initViewControllerWith(identifier: "FollowingListViewController", and: "")]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        // Create bar
        let bar = TMBar.ButtonBar()
        
        bar.layout.contentInset = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: -10.0)
        bar.layout.transitionStyle = .snap // Customize
        bar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        bar.backgroundView.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        bar.indicator.overscrollBehavior = .bounce
        bar.indicator.weight = .medium
        bar.indicator.tintColor = #colorLiteral(red: 0.9104420543, green: 0.22964257, blue: 0.2734379768, alpha: 1)
        
        bar.indicator.weight = .medium
        
        bar.buttons.customize { (button) in
            button.tintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            
            button.selectedTintColor = #colorLiteral(red: 0.9104420543, green: 0.22964257, blue: 0.2734379768, alpha: 1)
            
        }
        
        
        addBar(bar, dataSource: self, at: .top)
        
        let rightbutton = UIButton(type: .custom)
        rightbutton.setImage( UIImage(named: "menuItem") , for: .normal)
        rightbutton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        rightbutton.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        
        let rightBarButton = UIBarButtonItem(customView: rightbutton)
        let leftBbutton = UIButton(type: .custom)
        
        leftBbutton.setImage( UIImage(named: "backIcon") , for: .normal)
        leftBbutton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        leftBbutton.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        
        let leftBarButton = UIBarButtonItem(customView: leftBbutton)
        self.navigationItem.rightBarButtonItem = rightBarButton
        self.navigationItem.leftBarButtonItem = leftBarButton
        
    }
    
    @objc func addTapped(){
        
    }
}
extension MainMenuAccountViewController: PageboyViewControllerDataSource, TMBarDataSource {
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return viewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        return viewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return .first
    }
    
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        let title = menuItem[index]
        return TMBarItem(title: title)
    }
}
