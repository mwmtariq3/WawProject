//
//  FollowingListViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/16/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//


import UIKit

class FollowingListViewController: UIViewController {
    
    @IBOutlet weak var followingTabelView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        followingTabelView.register(FollowingTabelViewCell.self)
    }
}
extension FollowingListViewController: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
         return 10
     }
     
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         return 20.0
     }
     
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
         let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
         view.backgroundColor = .clear
         return view
     }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FollowingTabelViewCell = tableView.dequeueReusableCell(withIdentifier: "FollowingTabelViewCell") as! FollowingTabelViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}
