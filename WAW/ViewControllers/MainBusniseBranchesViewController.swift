//
//  MainBusniseBranchesViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/14/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//


import UIKit

class MainBusniseBranchesViewController: UIViewController {
    
    @IBOutlet weak var countryTextFiled: UITextFiledWithIcon!
    @IBOutlet weak var weekDayCollectionView: UICollectionView!
    @IBOutlet weak var hightOfTabelView: NSLayoutConstraint!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var countryTabelView: UITableView!
    @IBOutlet weak var countrySearchBar: UITextFiledWithIcon!
    @IBOutlet weak var viewOfSearch: UIView!
    
    var selectedDay = ""
    var valueWeekDay = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
    var selectedCountry = ""
    
    var searchText = String() {
        didSet{
            if searchText == "" {
                self.dataArray = self.optionArray
            }else{
                self.dataArray = optionArray.filter {
                    return $0.range(of: searchText, options: .caseInsensitive) != nil
                }
            }
            setTableViewHeight()
        }
    }
    
    var dataArray:[String] = []
    var optionArray = [String]() {
        didSet{
            self.dataArray = self.optionArray
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weekDayCollectionView.register(WeekDayCollectionViewCell.self)
        countryTabelView.register(TitleTabelViewCell.self)
        optionArray = ["ABCD","ABC","AB","A","ABCD","ABC","AB","A","ABCD","ABC","AB","A","ABCD","ABC","AB","A","ABCD","ABC","Tariq","AB","A","ABCD","ABC","AB","A","ABCD","ABC","AB","A","ABCD","ABC","AB","A"]
        self.dataArray = optionArray
        
    }
    func setTableViewHeight(){
        let lableHeightAndMargeins: CGFloat = 220
        let maxHeight = view.frame.height - lableHeightAndMargeins
        let contentHeight = ((dataArray.count+1) * 50 )
        hightOfTabelView.constant = contentHeight > Int(maxHeight) ? maxHeight : CGFloat(contentHeight)
        self.countryTabelView.reloadData()
    }
    @IBAction func selectCountryAction(_ sender: CheckBox) {
        searchText = ""
        if !dataArray.isEmpty{
            self.viewOfSearch.isHidden = sender.isChecked
        }
        
    }
    
    @IBAction func didBeginEdit(_ sender: UITextFiledWithIcon) {
        searchText = sender.text ?? ""
    }
    @IBAction func didChangeText(_ sender: UITextFiledWithIcon) {
        searchText = sender.text ?? ""
    }
    
}
extension MainBusniseBranchesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return valueWeekDay.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: WeekDayCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.titleLabel.text = valueWeekDay[indexPath.row]
        cell.backgroundColor =  selectedDay == valueWeekDay[indexPath.row] ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0.9187511802, green: 0.1077247933, blue: 0.2354588509, alpha: 1)
        cell.titleLabel.textColor =  selectedDay == valueWeekDay[indexPath.row] ?  #colorLiteral(red: 0.4549019608, green: 0.4549019608, blue: 0.4549019608, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return cell
    }
    
    func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAt indexPath:IndexPath) -> CGSize
    {
        return CGSize(width: 38, height: 43)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedDay = valueWeekDay[indexPath.row]
        collectionView.reloadData()
    }
}
extension MainBusniseBranchesViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TitleTabelViewCell = tableView.dequeueReusableCell(withIdentifier: "TitleTabelViewCell") as! TitleTabelViewCell
            cell.backgroundColor = dataArray[indexPath.row] != selectedCountry ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
      
        
        cell.titleLabel.text = "\(dataArray[indexPath.row])"
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countryTextFiled.text = self.dataArray[indexPath.row]
        searchText.removeAll()
        countrySearchBar.text?.removeAll()
        viewOfSearch.isHidden = true
        selectedCountry = dataArray[indexPath.row]
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
