//
//  LanguagePageViewController.swift
//  WAW
//
//  Created by Tariq Al-aqrabawi on 8/10/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit

class LanguagePageViewController: UIViewController {
    
    @IBOutlet weak var arabicButton: UIButton!
    @IBOutlet weak var englishButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func selectLanguageAction(_ sender: UIButton) {
        switch sender {
        case arabicButton:
            print("")
        case englishButton:
            print("")
        default:
            print("")
        }
        let vc = initViewControllerWith(identifier: "NavLogin", and: "")
        vc.modalPresentationStyle = .fullScreen
        present(vc)
    }
}
