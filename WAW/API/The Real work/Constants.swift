//
//  Constants.swift
//  Mahalkom
//
//  Created by Wafaa Alshahed on 12/11/18.
//  Copyright © 2018 Line. All rights reserved.
//

import UIKit

class Constants {
    static let baseurl  = "http://207.180.220.206:8030/"
    static let api      = "API/WOW/"
    static let fullUrl      = baseurl + api
    static let deviceId = UIDevice.current.identifierForVendor!.uuidString
    static var countryList:[Country] = []
    struct urls {
        
        struct authUrls {
            static let login                                                = fullUrl + "login"
            static let register                                                = fullUrl + "register"
            static let logout                                                = fullUrl + "logout"
            static let registerBO                                                = fullUrl + "registerBO"
            static let checkduplicatedata                                                = fullUrl + "checkduplicatedata"
            
            static let addUpdateFCMToken                                                = fullUrl + "AddUpdateFCMToken"
            static let getGuestUser                                                = fullUrl + "GetGuestUser"
            static let updateGuestUserToken                                                = fullUrl + "UpdateGuestUserToken"
            static let resendActivationCode                                                = fullUrl + "ResendActivationCode"
            static let activateUserByActivationCode                                                = fullUrl + "ActivateUserByActivationCode"
            static let setLang                                                = fullUrl + "SetLang"
            
            static let testCodeActivation                                                = fullUrl + "TestCodeActivation"
            static let forgetPassword                                                = fullUrl + "ForgetPassword"
            
            static let forgetPasswordVerification                                                = fullUrl + "ForgetPasswordVerification"
            static let changePasswordAfterVerification                                                = fullUrl + "ChangePasswordAfterVerification"
            
        }
        
        struct listening {
            static let getCountry                                                = fullUrl + "GetCountry"
            static let getCities                                                = fullUrl + "GetCities"
            static let getMainCategory                                                = fullUrl + "GetMainCategory"
            static let getIndustry                                                = fullUrl + "GetIndustry"
            static let getAdvertisementCategory                                                = fullUrl + "GetAdvertisementCategory"
        }
        
        struct mainPage {
            static let getSlider                                                = fullUrl + "GetSlider"
            static let getMainCatSlider                                                = fullUrl + "GetMainCatSlider"
            static let getSubCatSlider                                                = fullUrl + "GetSubCatSlider"
            static let getWoWDealsSlider                                                = fullUrl + "GetWoWDealsSlider"
            
        }
        
        struct publicApi  {
            static let policy                                                = fullUrl + "Policy"
        }
        
        struct User {
            static let userProfile                                                = fullUrl + "UserProfile"
            static let addOrEditNewBussnies                                                = fullUrl + "AddOrEditNewBussnies"
            static let addOrEditBussniesBranch                                                = fullUrl + "AddOrEditBussniesBranch"
            static let addOrEditBussniesBranchDetails                                                = fullUrl + "AddOrEditBussniesBranchDetails"
            static let listOfBussniesInfo                                                = fullUrl + "ListOfBussniesInfo"
            static let listOfBussniesWithBranchesDetailsInfo = fullUrl + "ListOfBussniesWithBranchesDetailsInfo"
            static let listOfBussniesBranch = fullUrl + "ListOfBussniesBranch"
            static let listOfBussniesBranchDetails = fullUrl + "ListOfBussniesBranchDetails"
            static let loginAndAccountInfo = fullUrl + "LoginAndAccountInfo"
            static let updateLoginInfoPassword = fullUrl + "UpdateLoginInfoPassword"
            static let updateLoginPersonalInfo = fullUrl + "UpdateLoginPersonalInfo"
        }
        
        
    }
    enum UserType:String {
        case Guest = "Guest"
        case Normal = "Normal"
        case Advertiser = "Adveriser"
    }
    static var duplicateEmailAddress = "Email.Duplicate"
    static var verification = "Admin.User.Need Vrification Code"
    static var alreadyActive = "Admin.User.Already Active"
    static var inCorrectCredential = "InCorrectCredential"
    static var notActive = "NotActive"
    static var needVrificationCode = "NeedVrificationCode"
}
