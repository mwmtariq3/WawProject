

import Foundation

/*** Check if a certain string is empty
 ***/
extension String {
    func isEmpty() -> Bool{
        let strTmp = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return strTmp.isEmpty
    }

    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self) 

    }
    //validate PhoneNumber
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count && self.count >= 8
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}



