//
//  RequestManager.swift
//
//  Copyright © 2019 Line. All rights reserved.
//

import Foundation
import Alamofire
import LanguageManager_iOS

class RequestManager {
    static let shared = RequestManager()
    func headers() -> HTTPHeaders {
        var headers: HTTPHeaders = [
            "Content-Type": " application/json-patch+json",
            "accept": "application/json",
            "Accept-Language": LanguageManager.shared.currentLanguage == .ar ? "ar" : "en"
        ]
        //Add the user token to header if any
        guard let userToken: String = Helper.userToken  else {
            return headers
        }
        headers["Authorization"] = "Bearer" + " "  + userToken
        return headers
    }
    
    //Reachability
    let networkReachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.apple.com")
    
    func RequestConverter(_ endpoint:String , method:String ,queryParm : [String:Any] = [:], param: [String:Any]) -> URLRequestConvertible {
        let urlwithPercentEscapes = endpoint.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        
        var request = URLRequest(url: URL(string: urlwithPercentEscapes ?? "")! )
        request.httpMethod = method
        do{
            request = try URLEncoding.default.encode(request, with: queryParm)
        }catch{
        }
        request.allHTTPHeaderFields = RequestManager.shared.headers()
        if (param.count > 0) {
            request.httpBody = try! JSONSerialization.data(withJSONObject: param)
        }
        return request
    }
    
    //Post API
    func postAPI<T: Decodable>(_ url:String,hasLoading:Bool = true,viewController:UIViewController, decodable: T.Type, parameters: [String:Any],completionHandler: @escaping (_ Result: T?, Error?) -> Void)
    {
        if url.isEmpty {
            let error = NSError(domain: "Error: Empty url", code: -1, userInfo: [:])
            completionHandler(nil,error)
            return
        }
        if networkReachabilityManager?.isReachable ?? false == false {
            let error = NSError(domain: "يرجى فحص الانترنت والمحاولة لاحقا", code: -1, userInfo: [:])
            completionHandler(nil,error)
            return
        }
        let request = RequestManager.shared.RequestConverter(url, method: "POST", param: parameters)
        
        let api =  Alamofire.request(request)
        if hasLoading {
            viewController.showLoading()
        }
        api.responseJSON
            {
                response -> Void in
                viewController.stopLoading()
                switch response.result{
                case .success:
                    let responseDic = response
                    guard let responseDictionary  = responseDic.value as? NSDictionary else {
                        let error = NSError(domain: "Error: Can't convert to dict", code: -1, userInfo: [:])
                        completionHandler(nil,error)
                        return
                        
                    }
                        do {
                            guard let decode = try? JSONSerialization.data(withJSONObject: responseDictionary, options: .prettyPrinted)  else {
                                let error = NSError(domain: "Error: Can't convert to dict", code: -1, userInfo: [:])
                                completionHandler(nil,error)
                                return
                            }
                            let decoder = JSONDecoder.init()
                            let ObjectInfo = try decoder.decode(T.self, from: decode)
                            completionHandler(ObjectInfo, nil)
                        }catch (let error) {
                            let error = NSError(domain: error.localizedDescription, code: -1, userInfo: [:])
                            completionHandler(nil,error)
                        }
                    
                    break
                case .failure(let error):
                    let error = NSError(domain: error.localizedDescription , code: -1, userInfo: [:])
                    completionHandler(nil,error)
                    break
                }
        }
    }
    //Get API
    func
        getAPI<T: Decodable>(_ url:String,hasLoading:Bool = true,viewController:UIViewController, decodable: T.Type, parameters: [String:Any] = [:],completionHandler: @escaping (_ Result: T?, Error?) -> Void)
    {
        
        if url.isEmpty {
            let error = NSError(domain: "Error: Empty url", code: -1, userInfo: [:])
            completionHandler(nil,error)
            return
        }
        if networkReachabilityManager?.isReachable ?? false == false {
            let error = NSError(domain: "يرجى فحص الانترنت والمحاولة لاحقا", code: -1, userInfo: [:])
            completionHandler(nil,error)
            return
        }
        let request = RequestManager.shared.RequestConverter(url, method: "GET", queryParm: parameters, param: [:])
        if hasLoading {
            viewController.showLoading()
        }
        
        let api =  Alamofire.request(request)
        api.responseJSON { response in
            viewController.stopLoading()
            switch response.result{
            case .success:
                let responseDic = response
                guard let responseDictionary  = responseDic.value as? NSDictionary else {
                    let error = NSError(domain: "Error: Can't convert to dict", code: -1, userInfo: [:])
                    completionHandler(nil,error)
                    return
                    
                }
                if let errorList = responseDictionary["ErrorList"] as? [String] {
                    if errorList.count != 0 {
                        let error = NSError(domain: errorList.joined(separator: "\n"), code: -1, userInfo: [:])
                        completionHandler(nil,error)
                        return
                    }
                    do {
                        guard let decode = try? JSONSerialization.data(withJSONObject: responseDictionary, options: .prettyPrinted)  else {
                            let error = NSError(domain: "Error: Can't convert to dict", code: -1, userInfo: [:])
                            completionHandler(nil,error)
                            return
                        }
                        let decoder = JSONDecoder.init()
                        let ObjectInfo = try decoder.decode(T.self, from: decode)
                        completionHandler(ObjectInfo, nil)
                    }catch (let error) {
                        let error = NSError(domain: error.localizedDescription, code: -1, userInfo: [:])
                        completionHandler(nil,error)
                    }
                }else {
                    let error = NSError(domain: "Not have a key", code: -1, userInfo: [:])
                    completionHandler(nil,error)
                    viewController.stopLoading()
                }
                break
            case .failure(let error):
                let error = NSError(domain: error.localizedDescription , code: -1, userInfo: [:])
                completionHandler(nil,error)
                break
            }
        }
    }
    
    
    //Delete API
    func deleteAPI<T: Decodable>(_ url:String,viewController:UIViewController,hasLoading:Bool = true, decodable: T.Type,completionHandler: @escaping (_ Result: T?, Error?) -> Void)
    {
        
        if url.isEmpty {
            let error = NSError(domain: "Error: Empty url", code: -1, userInfo: [:])
            completionHandler(nil,error)
            return
        }
        if networkReachabilityManager?.isReachable ?? false == false {
            let error = NSError(domain: "يرجى فحص الانترنت والمحاولة لاحقا", code: -1, userInfo: [:])
            completionHandler(nil,error)
            return
        }
        if hasLoading {
            viewController.showLoading()
        }
        let request = RequestManager.shared.RequestConverter(url, method: "DELETE", param:[:])
        let api =  Alamofire.request(request)
        api.responseJSON { response in
            viewController.stopLoading()
            switch response.result{
            case .success:
                let responseDic = response
                guard let responseDictionary  = responseDic.value as? NSDictionary else {
                    let error = NSError(domain: "Error: Can't convert to dict", code: -1, userInfo: [:])
                    completionHandler(nil,error)
                    return
                    
                }
                if let errorList = responseDictionary["ErrorList"] as? [String] {
                    if errorList.count != 0 {
                        let error = NSError(domain: errorList.joined(separator: "\n"), code: -1, userInfo: [:])
                        completionHandler(nil,error)
                        return
                    }
                    do {
                        guard let decode = try? JSONSerialization.data(withJSONObject: responseDictionary, options: .prettyPrinted)  else {
                            let error = NSError(domain: "Error: Can't convert to dict", code: -1, userInfo: [:])
                            completionHandler(nil,error)
                            return
                        }
                        let decoder = JSONDecoder.init()
                        let ObjectInfo = try decoder.decode(T.self, from: decode)
                        completionHandler(ObjectInfo, nil)
                    }catch (let error) {
                        let error = NSError(domain: error.localizedDescription, code: -1, userInfo: [:])
                        completionHandler(nil,error)
                    }
                }else {
                    let error = NSError(domain: "Not have a key", code: -1, userInfo: [:])
                    completionHandler(nil,error)
                    viewController.stopLoading()
                    
                }
                break
            case .failure(let error):
                let error = NSError(domain: error.localizedDescription , code: -1, userInfo: [:])
                completionHandler(nil,error)
                break
            }
        }
    }
    
    
    func postRequestWithMultipart<T: Decodable>(_ url:String,viewController:UIViewController,hasLoading:Bool = true, decodable: T.Type, parameters:[String : Any], imagesArray:[Data]? = [], imageServerKey: [String]? = [],mimeType:[String] = ["image/jpeg"],extType:[String] = ["jpeg"] ,completionHandler: @escaping (_ Result: T?, Error?) -> Void)   {
        if url.isEmpty {
            let error = NSError(domain: "Error: Empty url", code: -1, userInfo: [:])
            completionHandler(nil,error)
            return
        }
        if networkReachabilityManager?.isReachable ?? false == false {
            let error = NSError(domain: "يرجى فحص الانترنت والمحاولة لاحقا", code: -1, userInfo: [:])
            completionHandler(nil,error)
            return
        }
        if hasLoading {
            viewController.showLoading()
        }
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let imageDataArray = imagesArray {
                    if imageDataArray.count > 0 {
                        for index  in   0 ... imageDataArray.count - 1 {
                            multipartFormData.append(imageDataArray[index], withName: imageServerKey?[index] ?? "", fileName: "\(Date().timeIntervalSince1970).\(extType[index])", mimeType:mimeType[index])
                        }
                    }
                    
                }
                
                
                for (key,value) in parameters {
                    if value is String || value is Int {
                        multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                    }
                    
                }
                
        },
            to: url,
            method: .post,
            headers:headers(),
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    //SUCCESS
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if response.result.isSuccess{
                            let responseDic = response
                            guard let responseDictionary  = responseDic.value as? NSDictionary else {
                                let error = NSError(domain: "Error: Can't convert to dict", code: -1, userInfo: [:])
                                completionHandler(nil,error)
                                return
                            }
                            if let errorList = responseDictionary["ErrorList"] as? [String] {
                                if errorList.count != 0 {
                                    let error = NSError(domain: errorList.joined(separator: "\n"), code: -1, userInfo: [:])
                                    completionHandler(nil,error)
                                    return
                                }
                                do {
                                    guard let decode = try? JSONSerialization.data(withJSONObject: responseDictionary, options: .prettyPrinted)  else {
                                        let error = NSError(domain: "Error: Can't convert to dict", code: -1, userInfo: [:])
                                        completionHandler(nil,error)
                                        return
                                    }
                                    let decoder = JSONDecoder.init()
                                    let ObjectInfo = try decoder.decode(T.self, from: decode)
                                    completionHandler(ObjectInfo, nil)
                                }catch (let error) {
                                    let error = NSError(domain: error.localizedDescription, code: -1, userInfo: [:])
                                    completionHandler(nil,error)
                                }
                                
                            }else {
                                let error = NSError(domain: "Not have a key", code: -1, userInfo: [:])
                                completionHandler(nil,error)
                                
                            }
                        } else {
                            _ = "ERROR MESSAGE: "
                            if let data = response.data {
                                do {
                                    let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: String]
                                    let error = NSError(domain: json.description, code: -1, userInfo: [:])
                                    completionHandler(nil,error)
                                } catch let myJSONError {
                                    print(myJSONError)
                                }
                            }
                        }
                    }
                    
                //FAILURE
                case .failure(let encodingError):
                    print("FALLE ------------")
                    print(encodingError)
                    completionHandler(nil,encodingError)
                }
        }
        )
    }
}
