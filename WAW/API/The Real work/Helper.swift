

import UIKit
import NVActivityIndicatorView
import Alamofire
class Helper: NSObject   {
    
    
    static func CellPhone(_ phone:String)  {
        if (phone == ""){
            return
        }
        let urlString:String = "tel://\(phone.replacedArabicDigitsWithEnglish)"
        if let phoneCallURL = URL(string: urlString) {
            if (UIApplication.shared.canOpenURL(phoneCallURL)) {
                UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    static func OpenMap(_ lat:Double = 31.9989997 , _ lng:Double = 31.9989997) {
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
        {
            let urlString = "http://maps.google.com/?daddr=\(lat),\(lng)&directionsmode=driving"
            if let url = URL(string: urlString) {
                UIApplication.shared.open(url)
            }
            
        }
        else
        {
            let urlString = "http://maps.apple.com/?address=\(lat),\(lng)"
            if let url = URL(string: urlString) {
                UIApplication.shared.open(url)
            }        }
    }
    
    static func rootWindow() -> UIWindow? {
        let _appDelegator = UIApplication.shared.delegate! as! AppDelegate
        var sceneDelegate:SceneDelegate?
        if  _appDelegator.window != nil {
            return _appDelegator.window!
        }else {
            if #available(iOS 13.0, *) {
                sceneDelegate = UIApplication.shared.connectedScenes
                    .first?.delegate as? SceneDelegate
                return  sceneDelegate?.window
            }
        }
        return nil
    }
    static var currentUser:User? {
        get {
            var user:User?
            if let userArr  = UserDefaults.standard.value(forKey: "currentUser") as? [String:Any]   {
                do {
                    let data = try JSONSerialization.data(withJSONObject: userArr, options: .prettyPrinted)
                    let decode = JSONDecoder.init()
                    user = try decode.decode(User.self, from: data)
                    
                    return user!
                }catch(let err){
                    print(err)
                }
            }
            return user
        }
        set{
            do {
                let defaults = UserDefaults.standard
                defaults.set(try newValue.asDictionary(), forKey: "currentUser")
                defaults.synchronize()
            }catch(let err) {
                print(err)
            }
        }
    }
    static var OtherUser:User? {
        get {
            var user:User?
            if let userArr  = UserDefaults.standard.value(forKey: "OtherUser") as? [String:Any]   {
                do {
                    let data = try JSONSerialization.data(withJSONObject: userArr, options: .prettyPrinted)
                    let decode = JSONDecoder.init()
                    user = try decode.decode(User.self, from: data)
                    
                    return user!
                }catch(let err){
                    print(err)
                }
            }
            return user
        }
        set{
            do {
                let defaults = UserDefaults.standard
                defaults.set(try newValue.asDictionary(), forKey: "OtherUser")
                defaults.synchronize()
            }catch(let err) {
                print(err)
            }
        }
    }
    static func ContinueUserLoggedIn(viewController:UIViewController,completionHandler: @escaping (_ Success:Bool, Error?) -> Void, Error: @escaping (String?) -> Void)  {
        let param:[String:Any] = ["userId":Helper.currentUser?.id ?? -1 ]
        viewController.showLoading()
        RequestManager.shared.postAPI(Constants.urls.authUrls.login, viewController: viewController, decodable:GenericModel<User>.self , parameters: param) { (userInfo, error) in
            
            if userInfo?.msg?.count ?? 0  == 0  {
                Helper.currentUser = userInfo?.data
                completionHandler(true,nil)
            }else {
                completionHandler(false,error)
            }
            
        }
    }
    static func isLoggingUser() -> Bool {
        guard let token = userToken else {
            return false
        }
        return !token.isEmpty
    }
    static func resetLoggingData() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey:"currentUser")
        defaults.removeObject(forKey:"token")
    }
    
    static func toDate(_ dateTimeStamp:Double , formate:String = "MMM.dd-yyyy") ->String
    {
        
        let date = NSDate(timeIntervalSince1970: dateTimeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
        
        var dateString = dayTimePeriodFormatter.string(from: date as Date)
        dayTimePeriodFormatter.dateFormat = formate
        dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        return dateString
        
    }
    
    
       static func initViewControllerWith(identifier:String,and title:String,storyboardName:String = "UserAccount") -> UIViewController{
         let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: identifier)
         vc.title = title
         vc.navigationItem.title = title
         return vc
     }
    
    static var isFirstLaunch:Bool? {
        get{
            let firstLunch = UserDefaults.standard.bool(forKey: "isFirstLaunch") 
            return firstLunch
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "isFirstLaunch")
        }
    }
    static var userType:String? {
        get{
            let firstLunch = UserDefaults.standard.string(forKey: "userType")
            return firstLunch 
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "userType")
        }
    }

    static var userToken:String?{
        get{
            let token = UserDefaults.standard.string(forKey: "token") ?? ""
            return token
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "token")
        }
        
    }
    
    static func toTimeStamp(_ timeString:String) -> Int {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="dd/MM/yyyy"
        let date = dfmatter.date(from: timeString )
        if date == nil {return 0}
        let dateStamp:TimeInterval = date!.timeIntervalSince1970
        let dateSt:Int = Int(dateStamp)
        return dateSt
    }
    
    
}
