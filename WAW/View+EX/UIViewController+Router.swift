//
//  UIViewController+Router.swift
//  Blink
//
//  Created by Anas Alhasani on 2/26/19.
//  Copyright © 2019 Blink. All rights reserved.
//

import UIKit
import SafariServices
import NVActivityIndicatorView
import AwaitToast
import YPImagePicker
import RSSelectionMenu
import LanguageManager_iOS
import Firebase
import FirebaseCore
import FirebaseMessaging

extension UIViewController {
    public func present<T: UIViewController>(
        _ viewController: T,
        animated: Bool = true,
        modalPresentationStyle: UIModalPresentationStyle? = nil,
        configure: ((T) -> Void)? = nil,
        completion: ((T) -> Void)? = nil
    ) {
        if let modalPresentationStyle = modalPresentationStyle {
            viewController.modalPresentationStyle = modalPresentationStyle
        }
        
        configure?(viewController)
        present(viewController, animated: animated) {
            completion?(viewController)
        }
    }
    //present viewController With Navigation
    //you can use it directly from viewController
    public func presentWithNavigation<T: UIViewController>(
        _ viewController: T,
        animated: Bool = true,
        configure: ((T) -> Void)? = nil,
        completion: ((T) -> Void)? = nil
    ) {
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = modalPresentationStyle
        configure?(viewController)
        present(navigationController, animated: animated) {
            completion?(viewController)
        }
    }
    //show viewController
    //you can use it directly from viewController
    //it use related navigation Controller
    public func show<T: UIViewController>(
        _ viewController: T,
        animated: Bool = true,
        configure: ((T) -> Void)? = nil
    ) {
        let barButtonItem = UIBarButtonItem()
        barButtonItem.title = ""
        navigationItem.backBarButtonItem = barButtonItem
        viewController.navigationItem.backBarButtonItem = barButtonItem
        viewController.hidesBottomBarWhenPushed = true
        viewController.modalPresentationStyle = modalPresentationStyle
        
        configure?(viewController)
        navigationController?.pushViewController(viewController, animated: animated)
    }
    func navTitleWithImageAndText(titleText: String, imageView: UIImage?) -> UIView {
        
        // Creates a new UIView
        let titleView = UIView()
        
        // Creates a new text label
        let label = UILabel()
        label.text = titleText
        label.sizeToFit()
        label.font = AppFont.regular.size(15, TypeFont: .nexafamilyName)
        label.textColor = #colorLiteral(red: 0.9098039216, green: 0.2274509804, blue: 0.2705882353, alpha: 1)
        label.center = titleView.center
        label.textAlignment = NSTextAlignment.center
        
        // Creates the image view
        let imageItem = UIImageView()
        imageItem.image = imageView
        imageItem.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        // Maintains the image's aspect ratio:
        if let image = imageItem.image {
            let imageAspect = image.size.width / image.size.height
            
            // Sets the image frame so that it's immediately before the text:
            let imageX = (label.frame.origin.x - label.frame.size.height * imageAspect) - 10
            let imageY = label.frame.origin.y
            
            let imageWidth = label.frame.size.height * imageAspect
            let imageHeight = label.frame.size.height
            
            imageItem.frame = CGRect(x: imageX, y: imageY, width: imageWidth, height: imageHeight)
            
            imageItem.contentMode = UIView.ContentMode.scaleAspectFit
            titleView.addSubview(label)
            titleView.addSubview(imageItem)
        }else {
            titleView.addSubview(label)
            
        }
        
        // Adds both the label and image view to the titleView
        
        
        // Sets the titleView frame to fit within the UINavigation Title
        titleView.sizeToFit()
        
        return titleView
        
    }
    public func dismissViewController(animated: Bool = true) {
        guard let navigationController = navigationController else {
            dismiss(animated: animated)
            return
        }
        
        guard navigationController.viewControllers.count > 1 else {
            return navigationController.dismiss(animated: animated)
        }
        
        navigationController.popViewController(animated: animated)
    }
    // create instance from viewController or any destination file for example tabBarViewController
    public func initViewControllerWith(identifier: String, and title: String, storyboardName: String = "Main") -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: identifier)
        vc.title = title
        return vc
    }
    
    func getCountry(completion: (() -> ())? = nil) {
        SendAPIGet(url: Constants.urls.listening.getCountry, decodable: GenericModel<[Country]>.self) { (response) in
            Constants.countryList = response.data ?? []
            completion?()
        }
    }
    func getCityById(cityId:Int,completion: (([City]) -> ())? = nil) {
        SendAPIGet(url: Constants.urls.listening.getCities,data:["countryId":cityId], decodable: GenericModel<[City]>.self) { (response) in
            completion?(response.data ?? [])
        }
    }
    
    @objc func back() {
        navigationController?.popViewController(animated: true)
        
    }
    func presentHome() {
        self.view.window?.rootViewController?.dismiss(animated: false, completion: nil)
        let vc = initViewControllerWith(identifier: "NavHomeViewController", and: "")
        vc.modalPresentationStyle = .fullScreen
        self.view.window?.rootViewController = vc
    }
    
    func Logout(dict: [String: Any]) {
        SendAPIPost(url: Constants.urls.authUrls.logout, data: dict, decodable: GenericModel<Empty>.self) { (response) in
            if response.msg?.count ?? 0 != 0 {
                self.showToast(message: response.msg?.joined(separator: "\n") ?? "")
                return
            }
            Helper.currentUser = nil
            Helper.userToken = nil
            self.presentHome()
        }
    }
    func AuthNormal(dict: [String: Any],viewController:UIViewController, isLogin: Bool, completion: (() -> ())? = nil) {
        SendAPIPost(url: isLogin ? Constants.urls.authUrls.login : Constants.urls.authUrls.register, data: dict, decodable: GenericModel<User>.self) { (response) in
            if response.msg?.count ?? 0 != 0 {
                if response.msg?.contains(Constants.duplicateEmailAddress) ?? false {
                    self.showalert(title: "Email Duplicate", message: "Sorry, that email already exists , Do you want to log in?", okTitleButton: "Yes", cancelTitleButton: "No") {
                        MainLoginViewController.openLogin = true
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }else if response.msg?.contains(Constants.verification) ?? false {
                    let vc = self.initViewControllerWith(identifier: "VerficationPageViewController", and: "") as! VerficationPageViewController
                    vc.email = dict["EmailOrPhoneNumber"] as? String ?? ""
                    viewController.show(vc)
                }else {
                    self.showToast(message: response.msg?.joined(separator: "\n") ?? "")
                    
                }
                return
            }
            Helper.currentUser = response.data
            self.presentHome()
        }
    }
    func updateFCMToken(completion: (() -> ())? = nil) {
        SendAPIPost(url: Constants.urls.authUrls.addUpdateFCMToken, data: ["deviceId":Constants.deviceId,"fcmToken":Messaging.messaging().fcmToken ?? ""],decodable:GenericModel<Empty>.self) {(response) in
            if response.msg?.count ?? 0 != 0 {
                self.showToast(message: response.msg?.joined(separator: "\n") ?? "")
                return
            }
            completion?()
        }
    }
    func AuthBO(dict: [String: Any], completion: (() -> ())? = nil) {
        SendAPIPost(url: Constants.urls.authUrls.registerBO, data: dict, decodable: GenericModel<User>.self) { (response) in
            if response.msg?.count ?? 0 != 0 {
                self.showToast(message: response.msg?.joined(separator: "\n") ?? "")
                return
            }
            Helper.currentUser = response.data
            completion?()
        }
    }
    
    func SendAPIPost<T: Decodable>(url:String,data: [String: Any],decodable:  T.Type, completion: ((T) -> ())? = nil) {
        RequestManager.shared.postAPI( url, viewController: self, decodable: T.self, parameters: data) { (responseObject, error) in
            if error != nil {
                self.showToast(message: error?.localizedDescription ?? "")
                return
            }
            guard responseObject != nil else {
                self.showToast(message: "حدث خطأ ما ... يرجى المحاولة لاحقاً")
                return
            }
            
            
            completion?(responseObject!)
        }
    }
    
    func SendAPIGet<T: Decodable>(url:String,data: [String: Any] = [:],decodable: T.Type, completion: ((T) -> ())? = nil) {
        RequestManager.shared.getAPI(url, viewController: self, decodable: T.self, parameters: data) { (responseObject, error) in
            if error != nil {
                self.showToast(message: error?.localizedDescription ?? "")
                return
            }
            guard responseObject != nil else {
                self.showToast(message: "حدث خطأ ما ... يرجى المحاولة لاحقاً")
                return
            }
            
            completion?(responseObject!)
        }
    }
}

extension UIViewController: NVActivityIndicatorViewable {
    func setStatusBar(color: UIColor) {
        let tag = 12321
        if let taggedView = self.view.viewWithTag(tag) {
            taggedView.removeFromSuperview()
        }
        let overView = UIView()
        overView.frame = UIApplication.shared.statusBarFrame
        overView.backgroundColor = color
        overView.tag = tag
        self.view.addSubview(overView)
    }
    func showalert(title:String , message:String , okTitleButton:String, cancelTitleButton:String,completion: (() -> ())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: okTitleButton, style: .default, handler: { (okAlert) in
            guard let completion = completion else {
                alert.dismissViewController()
                return
            }
            completion()
        }))
        alert.addAction(UIAlertAction(title: cancelTitleButton, style: .cancel, handler: { (cancelAlert) in
            alert.dismissViewController()
        }))
        
        self.present(alert, animated: true)
    }
    func showLoading() {
        startAnimating(type: .ballRotateChase)
    }
    
    func stopLoading() {
        stopAnimating()
    }
    func showToast(message: String, duration: TimeInterval = 3.0) {
        ToastAppearanceManager.default.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        ToastAppearanceManager.default.numberOfLines = 0
        ToastAppearanceManager.default.height = AutomaticDimension
        ToastBehaviorManager.default.duration = duration
        ToastAppearanceManager.default.titleEdgeInsets = .init(top: 8, left: 20, bottom: 8, right: 20)
        ToastAppearanceManager.default.textColor = .white
        let toast = Toast.default(text: message, direction: .top)
        
        toast.show()
        
    }
    
}

extension UINavigationBar {
    func transparentNavigationBar() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
        self.backgroundColor = .clear
    }
}

extension UINavigationController {
    func push(_ vc:UIViewController)  {
        self.pushViewController(vc, animated: true)
    }
    func clearBackground() {
        self.view.backgroundColor = .clear
        
    }
}
extension String {
    
    public var replacedArabicDigitsWithEnglish: String {
        var str = self
        let map = ["٠": "0",
                   "١": "1",
                   "٢": "2",
                   "٣": "3",
                   "٤": "4",
                   "٥": "5",
                   "٦": "6",
                   "٧": "7",
                   "٨": "8",
                   "٩": "9"]
        map.forEach { str = str.replacingOccurrences(of: $0, with: $1) }
        return str
    }
}
