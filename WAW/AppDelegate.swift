//
//  AppDelegate.swift
//  WAW
//
//  Created by Abed AlRhman Qasim on 5/31/20.
//  Copyright © 2020 Abed AlRhman Qasim. All rights reserved.
//

import UIKit
import LanguageManager_iOS
import IQKeyboardManagerSwift
import CoreData
import Firebase
import FirebaseAuth
import GoogleSignIn
import FBSDKCoreKit
import UserNotifications
import Messages
import FirebaseCore
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var fcmToken = ""
    var window: UIWindow?
    var isBackground = false
    var type:String = ""
    var Id:Int = -1
    var userInfo: NSDictionary = [:]
    var notificationCenter = UNUserNotificationCenter.current()
    var googleClientId = "625909814040-bdshlqc4i11fa1mtkjonau8j7nj7qmcs.apps.googleusercontent.com"
    
    var facebookClientID = "216290096492248"
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        LanguageManager.shared.defaultLanguage = .en
        LanguageManager.shared.currentLanguage = .en
        
        FirebaseApp.configure()
        
        IQKeyboardManager.shared.enable = true
        settingPushNotification()
        printFonts()
        changeFont()
        setupLoginWithSocial()
        return true
    }
    func setupLoginWithSocial()  {
        GIDSignIn.sharedInstance().clientID = googleClientId
    }
    func setApperance(){
         UINavigationBar.appearance().barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
         UINavigationBar.appearance().tintColor =  #colorLiteral(red: 0.9104420543, green: 0.22964257, blue: 0.2734379768, alpha: 1)
         UINavigationBar.appearance().isTranslucent = false
         UINavigationBar.appearance().shadowImage = UIImage()
//         UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font:UIFont(name: Constants.strings.fontName, size: 17)!,NSAttributedString.Key.foregroundColor:UIColor.white]
         
    
     }
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any])
        -> Bool {
            let facebookResponse = ApplicationDelegate.shared.application(application, open: url, options: options)
            let googleResponse = GIDSignIn.sharedInstance().handle(url)
            return facebookResponse || googleResponse
    }
    func settingPushNotification() {
        let app = UIApplication.shared
        
        //UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (isAllow, error) in
            
            if isAllow {
                
                Messaging.messaging().delegate = self
                
            }
            
        }
        app.registerForRemoteNotifications()
    }
    func changeFont() {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9098039216, green: 0.2274509804, blue: 0.2705882353, alpha: 1)
            , NSAttributedString.Key.font: AppFont.regular.size(18, TypeFont: LanguageManager.shared.currentLanguage == .ar ? .helveticaFamilyName : .nexafamilyName)]
    }
    func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Successfully registered for notifications!")
        print(token)
        if !token.isEmpty {
            
            let userDefaults = UserDefaults.standard
            userDefaults.set(token, forKey: "Token")
            
        }
        
        
        print("Device Token: \(token)")
    }
    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
        }
    }
    func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(userInfo)
        self.scheduleNotification(userI: userInfo)
    }
    
}
extension AppDelegate : MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("fcmToken : \(fcmToken)")
        
    }
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func clickByType(type: String, Id: Int) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
    }
    
    func scheduleNotification(userI: NSDictionary?) {
        NotificationCenter.default.post(name: Notification.Name("ChangeDot"), object: nil,userInfo: ["Changes":true])
        let data = (userI?["gcm.notification.data"] as? String)?.convertToDictionary()
        if data?.count ?? 0 > 0 {
            self.Id = data?["Id"] as? Int ?? -1
            
        }else {
            self.Id = userI?["Id"] as? Int ?? -1
        }
        
        if Id != -1 {
            
            self.clickByType(type:type,Id: Id)
            
        }
        
        
    }
    
}
extension AppDelegate {
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("APP_ENTER_IN_BACKGROUND")
        isBackground = true
        NotificationCenter.default.post(name: Notification.Name(rawValue: "APP_ENTER_IN_BACKGROUND"), object: nil)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("APP_ENTER_IN_FOREGROUND")
        isBackground = false
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "APP_ENTER_IN_FOREGROUND"), object: nil)
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
        userInfo = notification.request.content.userInfo as NSDictionary
        
        
    }
    
}
extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
